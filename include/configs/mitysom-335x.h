/*
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#ifndef __CONFIG_MITYSOM335X_H
#define __CONFIG_MITYSOM335X_H

#include <configs/ti_am335x_common.h>

#define MACH_TYPE_MITYSOM335X		3903
#define CONFIG_MACH_TYPE		MACH_TYPE_MITYSOM335X

/* Clock related defines */
#define V_OSCK				24000000 /* Clock output from T2 */
#define V_SCLK				(V_OSCK)

#define CONFIG_CMD_STRINGS

/* Always 128 KiB env size */
#define CONFIG_ENV_SIZE  (128 << 10)

/* Disable SPL direct boot */
/* When booting from nand, this will skip 2nd level u-boot. Would take some
 * setup to get to work.  Disabled to keep old usage. */
#undef CONFIG_SPL_OS_BOOT

/* Add support for booting debian initramfs */
#define CONFIG_SUPPORT_RAW_INITRD

/* Set correct loadaddr for u-boot, used during usb spl boot */
#undef CONFIG_SYS_LOAD_ADDR
#define CONFIG_SYS_LOAD_ADDR 0x807fffc0

/*
 * memtest works on 64 MB in DRAM after skipping 64 from
 * start addr of ram disk
 */
#define CONFIG_CMD_MEMTEST
#define CONFIG_SYS_MEMTEST_START       (CONFIG_SYS_SDRAM_BASE \
					+ (64 * 1024 * 1024))
#define CONFIG_SYS_MEMTEST_END         (CONFIG_SYS_MEMTEST_START \
                                        + (64 * 1024 * 1024))
#define CONFIG_SYS_MEMTEST_SCRATCH     (0x81000000)	/* dummy address */

#if defined(CONFIG_MITYSOM_512MB_NAND) || defined(CONFIG_MITYSOM_1GB_NAND)
#define ENV_NAND_ROOT "ubi0:rootfs rw ubi.mtd=8,4096"
#else
#define ENV_NAND_ROOT "ubi0:rootfs rw ubi.mtd=8,2048"
#endif

#define CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG

#define CONFIG_EXTRA_ENV_SETTINGS \
	"loadaddr=0x83000000\0" \
	"envloadaddr=0x82000000\0" \
	"fdt_high=0xFFFFFFFF\0" \
	"fdtaddr=0x88000000\0" \
	"script_addr=0x81900000\0" \
	"console=ttyS0,115200n8\0" \
	"mmc_dev=0\0" \
	"mmc_root=/dev/mmcblk0p2 rw\0" \
	"nand_root=" ENV_NAND_ROOT "\0" \
	"mmc_root_fs_type=ext3 rootwait\0" \
	"nand_root_fs_type=ubifs rootwait=1\0" \
	"nand_src_addr=" CONFIG_NAND_SRC_ADDR "\0" \
	"nand_img_siz=0x500000\0" \
	"rootpath=/srv/nfs/mitysom\0" \
	"bootfile=fitImage\0" \
	"nfsopts=nolock\0" \
	"static_ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}" \
			"::off\0" \
	"ip_method=none\0" \
	"config_dvi=i2c dev 1;i2c mw 38 8 3d; i2c mw 38 33 0\0" \
	"bootenv=uEnv.txt\0" \
	"loadbootenv=load mmc ${mmc_dev} ${envloadaddr} ${bootenv}\0" \
	"importbootenv=echo Importing environment from mmc ...; " \
		"env import -t $envloadaddr $filesize\0" \
	"fit_setup=run fit_none; run fit_nand; run fit_nor\0" \
	"fit_nor=test -n ${nor_size} && " \
		"setenv fit_conf '#conf_'${nand_size}'_'${nor_size}\0" \
	"fit_nand=test -n ${nand_size} && " \
		"setenv fit_conf '#conf_'${nand_size}\0" \
	"fit_none=setenv fit_conf '#conf'\0" \
	"mmc_fitimage_boot=mmc rescan; " \
		"run fit_setup; " \
		"load mmc ${mmc_dev} ${loadaddr} fitImage &&" \
		"bootm ${loadaddr}${fit_conf}\0" \
	"mmc_zimage_boot=mmc rescan; " \
		"load mmc ${mmc_dev} ${loadaddr} zImage && " \
		"load mmc ${mmc_dev} ${fdtaddr} am335x-mitysom.dtb && " \
		"bootz ${loadaddr} - ${fdtaddr}\0" \
	"mmc_uimage_boot=mmc rescan; " \
		"load mmc ${mmc_dev} ${loadaddr} uImage && " \
		"bootm ${loadaddr}\0" \
	"optargs=video=da8xx:bpp=16\0" \
	"bootargs_defaults=setenv bootargs " \
		"console=${console} " \
		"${optargs}\0" \
	"mmc_args=run bootargs_defaults;" \
		"setenv bootargs ${bootargs} " \
		"root=${mmc_root} " \
		"rootfstype=${mmc_root_fs_type} ip=${ip_method}\0" \
	"nand_args=run bootargs_defaults;" \
		"setenv bootargs ${bootargs} " \
		"root=${nand_root} noinitrd " \
		"rootfstype=${nand_root_fs_type} ip=${ip_method}\0" \
	"net_args=run bootargs_defaults;" \
		"setenv bootargs ${bootargs} " \
		"root=/dev/nfs " \
		"nfsroot=${serverip}:${rootpath},${nfsopts} rw " \
		"ip=dhcp\0" \
	"netretry=yes\0" \
	"mmc_boot=run config_dvi; run mmc_args;" \
		"echo; echo Trying to boot fitImage from mmc ...; " \
		"run mmc_fitimage_boot; " \
		"echo; echo Trying to boot zImage from mmc ...; " \
		"run mmc_zimage_boot; " \
		"echo; echo Trying to boot uImage from mmc ...; " \
		"run mmc_uimage_boot;\0" \
	"nand_boot=echo Booting from nand ...; " \
		"run nand_args; " \
		"nand read.i ${loadaddr} ${nand_src_addr} ${nand_img_siz}; " \
		"run fit_setup; " \
		"bootm ${loadaddr}${fit_conf}; " \
		"bootm ${loadaddr}\0" \
	"net_boot=echo Booting from network ...; " \
		"setenv autoload no; " \
		"dhcp; " \
		"tftp ${loadaddr} ${bootfile}; " \
		"run net_args; " \
		"run fit_setup;" \
		"bootm ${loadaddr}${fit_conf};" \
		"bootm ${loadaddr};\0" \
	"altbootcmd=rtc_poweroff\0" \

#if defined(CONFIG_SPL_USBETH_SUPPORT)
#define CONFIG_BOOTCOMMAND \
	"echo Booting from network. Trying Ethernet then USB RNDIS.; " \
	"run net_boot;" \

#else
#define CONFIG_BOOTCOMMAND \
	"if mmc rescan; then " \
		"echo SD/MMC found on device ${mmc_dev};" \
		"if run loadbootenv; then " \
			"echo Loaded environment from ${bootenv};" \
			"run importbootenv;" \
		"fi;" \
		"if test -n $uenvcmd; then " \
			"echo Running uenvcmd ...;" \
			"run uenvcmd;" \
		"fi;" \
		"run mmc_boot;" \
	"fi;" \
	"run nand_boot;" \

#endif

/* Calls misc_init_r() in som.c */
#define CONFIG_MISC_INIT_R
/* Include commands icache/dcache */
#define CONFIG_CMD_CACHE


/* SPI support */
#ifdef CONFIG_SPI
# define CONFIG_SPI_FLASH
# define CONFIG_SPI_FLASH_STMICRO
# define CONFIG_CMD_SF
# define CONFIG_SF_DEFAULT_SPEED		48000000
# define CONFIG_SF_DEFAULT_BUS		1

/*
 * Default to using SPI for environment, etc.
 * 0x000000 - 0x020000 : SPL (128KiB)
 * 0x020000 - 0x0A0000 : U-Boot (512KiB)
 * 0x0A0000 - 0x0BFFFF : First copy of U-Boot Environment (128KiB)
 * 0x0C0000 - 0x0DFFFF : Second copy of U-Boot Environment (128KiB)
 * 0x0E0000 - 0x442000 : Linux Kernel
 * 0x442000 - 0x800000 : Userland
 */

/* SPI boot support */
# if defined(CONFIG_SPI_BOOT)
#  define CONFIG_ENV_IS_IN_SPI_FLASH
#  define CONFIG_SYS_REDUNDAND_ENVIRONMENT
#  define CONFIG_ENV_SPI_MAX_HZ		CONFIG_SF_DEFAULT_SPEED
#  define CONFIG_ENV_SECT_SIZE		(4 << 10) /* 4 KB sectors */
#  define CONFIG_ENV_OFFSET		0x0A0000
#  define CONFIG_ENV_OFFSET_REDUND	0x0C0000
#  define MTDIDS_DEFAULT			"nor0=m25p80-flash.0"
#  define MTDPARTS_DEFAULT		"mtdparts=m25p80-flash.0:128k(SPL)," \
					"512k(u-boot),128k(u-boot-env1)," \
					"128k(u-boot-env2),3464k(kernel)," \
					"-(rootfs)"
# endif /* CONFIG_SPI_BOOT */

# define CONFIG_SPL_SPI_SUPPORT
# define CONFIG_SPL_SPI_FLASH_SUPPORT
# define CONFIG_SPL_SPI_LOAD
# define CONFIG_ENV_SPI_BUS		0
# define CONFIG_ENV_SPI_CS		0
# define CONFIG_SYS_SPI_U_BOOT_OFFS	0x20000
#endif /* CONFIG_SPI */

/* PMIC support */
#define CONFIG_POWER_TPS65910

/* SPL */
#define CONFIG_SPL_LDSCRIPT		"arch/arm/mach-omap2/u-boot-spl.lds"

#ifdef CONFIG_NAND
#define CONFIG_SYS_NAND_5_ADDR_CYCLE
#define CONFIG_SYS_NAND_ONFI_DETECTION

/* Large page nand */
#if defined(CONFIG_MITYSOM_512MB_NAND) || defined(CONFIG_MITYSOM_1GB_NAND)
#define CONFIG_SYS_NAND_PAGE_SIZE       4096
#define CONFIG_SYS_NAND_OOBSIZE         224
#ifdef CONFIG_MITYSOM_1GB_NAND
#define CONFIG_SYS_NAND_BLOCK_SIZE      (128*CONFIG_SYS_NAND_PAGE_SIZE) /* 512K */
#else
#define CONFIG_SYS_NAND_BLOCK_SIZE      (64*CONFIG_SYS_NAND_PAGE_SIZE) /* 256K */
#endif
#define CONFIG_SYS_NAND_BAD_BLOCK_POS	NAND_LARGE_BADBLOCK_POS

#define CONFIG_SYS_NAND_ECCPOS {\
				2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,\
				16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,\
				28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,\
				40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,\
				52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,\
				64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,\
				76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,\
				88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,\
				100, 101, 102, 103, 104, 105, 106, 107, 108, 109,\
				110, 111, 112, 113, 114, 115, 116, 117, 118, 119,\
				120, 121, 122, 123, 124, 125, 126, 127, 128, 129,\
				130, 131, 132, 133, 134, 135, 136, 137, 138, 139,\
				140, 141, 142, 143, 144, 145, 146, 147, 148, 149,\
				150, 151, 152, 153, 154, 155, 156, 157, 158, 159,\
				160, 161, 162, 163, 164, 165, 166, 167, 168, 169,\
				170, 171, 172, 173, 174, 175, 176, 177, 178, 179,\
				180, 181, 182, 183, 184, 185, 186, 187, 188, 189,\
				190, 191, 192, 193, 194, 195, 196, 197, 198, 199,\
				200, 201, 202, 203, 204, 205, 206, 207, 208, 209}

#define CONFIG_SYS_NAND_ECCSIZE		512
#define CONFIG_SYS_NAND_ECCBYTES	26
#define CONFIG_NAND_OMAP_ECCSCHEME      OMAP_ECC_BCH16_CODE_HW
#define CONFIG_SYS_NAND_ECCSTEPS	8
#define	CONFIG_SYS_NAND_ECCTOTAL	(CONFIG_SYS_NAND_ECCBYTES * \
						CONFIG_SYS_NAND_ECCSTEPS)
#elif defined(CONFIG_MITYSOM_256MB_NAND)
#define CONFIG_SYS_NAND_PAGE_SIZE       2048
#define CONFIG_SYS_NAND_OOBSIZE         64
#define CONFIG_SYS_NAND_BLOCK_SIZE      (64*CONFIG_SYS_NAND_PAGE_SIZE)

#define CONFIG_SYS_NAND_BAD_BLOCK_POS	NAND_LARGE_BADBLOCK_POS
#define CONFIG_SYS_NAND_ECCPOS		{ 2, 3, 4, 5, 6, 7, 8, 9, \
					 10, 11, 12, 13, 14, 15, 16, 17, \
					 18, 19, 20, 21, 22, 23, 24, 25, \
					 26, 27, 28, 29, 30, 31, 32, 33, \
					 34, 35, 36, 37, 38, 39, 40, 41, \
					 42, 43, 44, 45, 46, 47, 48, 49, \
					 50, 51, 52, 53, 54, 55, 56, 57, }

#define CONFIG_SYS_NAND_ECCSIZE		512
#define CONFIG_SYS_NAND_ECCBYTES	14
#define CONFIG_NAND_OMAP_ECCSCHEME      OMAP_ECC_BCH8_CODE_HW
#define CONFIG_SYS_NAND_ECCSTEPS	4
#define	CONFIG_SYS_NAND_ECCTOTAL	(CONFIG_SYS_NAND_ECCBYTES * \
						CONFIG_SYS_NAND_ECCSTEPS)
#endif /* defined(CONFIG_MITYSOM_512MB_NAND) || defined(CONFIG_MITYSOM_1GB_NAND) */

#define CONFIG_SYS_NAND_PAGE_COUNT	(CONFIG_SYS_NAND_BLOCK_SIZE / \
					 CONFIG_SYS_NAND_PAGE_SIZE)

#undef CONFIG_SYS_NAND_U_BOOT_OFFS /* Clear default value */
#define CONFIG_SYS_NAND_U_BOOT_OFFS	(4 * CONFIG_SYS_NAND_BLOCK_SIZE) /* just after 4 x MLO copies*/
#define	CONFIG_SYS_NAND_U_BOOT_START	CONFIG_SYS_TEXT_BASE

#endif /* CONFIG_NAND */

/* NS16550 Configuration */
#define CONFIG_SYS_NS16550_COM1		0x44e09000	/* Baseboard has UART0 */
#define CONFIG_BAUDRATE			115200

/*
 * select serial console configuration
 */
#define CONFIG_SERIAL1			1
#define CONFIG_CONS_INDEX		1

/*
 * USB configuration
 */
#ifdef CONFIG_USB
#define CONFIG_USB_MUSB_DSPS
#define CONFIG_ARCH_MISC_INIT
#define CONFIG_USB_MUSB_PIO_ONLY
#define CONFIG_USB_GADGET_VBUS_DRAW	2
#define CONFIG_AM335X_USB0
#define CONFIG_AM335X_USB0_MODE	MUSB_PERIPHERAL
#define CONFIG_AM335X_USB1
#define CONFIG_AM335X_USB1_MODE MUSB_HOST
#endif
#ifdef CONFIG_MITYSOM_USBRNDIS_SUPPORT
#define CONFIG_USB_ETHER
#define CONFIG_USB_ETH_RNDIS
#define CONFIG_USBNET_HOST_ADDR	"de:ad:be:af:00:00"
#define CONFIG_SPL_NET_SUPPORT
#define CONFIG_SPL_NET_VCI_STRING "MitySOM 335x SPL Devkit"
#define CONFIG_USB_GADGET_DOWNLOAD
#endif


#if defined(CONFIG_SPL_BUILD) && defined(CONFIG_SPL_USBETH_SUPPORT)
/* disable host part of MUSB in SPL */
#undef CONFIG_USB_MUSB_HOST
#endif

/* Unsupported features */
#undef CONFIG_USE_IRQ
#if defined(CONFIG_NO_ETH)
# undef CONFIG_CMD_NET
# undef CONFIG_DRIVER_TI_CPSW
# undef CONFIG_MII
# undef CONFIG_BOOTP_DEFAULT
# undef CONFIG_BOOTP_DNS
# undef CONFIG_BOOTP_DNS2
# undef CONFIG_BOOTP_SEND_HOSTNAME
# undef CONFIG_BOOTP_GATEWAY
# undef CONFIG_BOOTP_SUBNETMASK
# undef CONFIG_NET_RETRY_COUNT		10
#endif

/* Network */
#if defined(CONFIG_CMD_NET)
# define CONFIG_PHY_GIGE
# define CONFIG_PHYLIB
# define CONFIG_PHY_ADDR			1 //Unused due to CPSW_SEARCH_PHY
# define CONFIG_PHY_SMSC
# define CONFIG_PHY_VITESSE
# define CONFIG_PHY_MICREL
#endif

/* NAND support */
#ifdef CONFIG_NAND
#define GPMC_NAND_ECC_LP_x8_LAYOUT	1

/* ENV in NAND */
#if defined(CONFIG_NAND)

#ifndef CONFIG_SPL_ENV_SUPPORT
#undef CONFIG_ENV_IS_NOWHERE
#define CONFIG_ENV_IS_IN_NAND
#endif

#define CONFIG_SYS_MAX_FLASH_SECT	2048            /* max no of sectors in a chip */

#ifdef CONFIG_MITYSOM_256MB_NAND
# define MNAND_ENV_OFFSET		(0x260000)       /* environment starts here */
# define CONFIG_SYS_ENV_SECT_SIZE	(128 << 10)	/* 128 KiB */
#endif

#ifdef CONFIG_MITYSOM_512MB_NAND
# define MNAND_ENV_OFFSET		(0x300000)       /* environment starts here */
# define CONFIG_SYS_ENV_SECT_SIZE	(256 << 10)	/* 256 KiB */
#endif

#ifdef CONFIG_MITYSOM_1GB_NAND
# define MNAND_ENV_OFFSET		(0x400000)       /* environment starts here */
# define CONFIG_SYS_ENV_SECT_SIZE	(512 << 10)	/* 512 KiB */
#endif

#define CONFIG_ENV_OFFSET		MNAND_ENV_OFFSET
#define CONFIG_ENV_ADDR			MNAND_ENV_OFFSET

/* Not using redundent flash ... just reserving the next block to be written if the first block is bad
#define CONFIG_ENV_OFFSET_REDUND        (0xA0000)
#define CONFIG_ENV_SIZE_REDUND          CONFIG_ENV_SIZE
*/
#endif  /* ENV in NAND */

/* defines for UBI FS in NAND */
#define CONFIG_MTD_PARTITIONS
#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_LZO
/* un-comment for UBIFS debug
#define CONFIG_UBIFS_FS_DEBUG
#define CONFIG_UBIFS_FS_DEBUG_MSG_LVL 3
#define CONFIG_UBIFS_FS_DEBUG_CHECKS
#define UBIFS_MSG_FLAGS_DEFAULT 0xFFFFFFFF
*/
#define MTDIDS_DEFAULT "nand0=nand"

#ifdef CONFIG_MITYSOM_256MB_NAND
# define MTDPARTS_DEFAULT "mtdparts=nand:512k@0(mlo),1920k@512k(uboot),128k@2432k(env),5m@2560k(kernel),97152k(userfs)"
#endif

#ifdef CONFIG_MITYSOM_512MB_NAND
#define MTDPARTS_DEFAULT "mtdparts=nand:1024k@0(mlo),2048k@1024k(uboot),256k@3072k(env),5m@3328k(kernel),515840k(userfs)"
#endif

#ifdef CONFIG_MITYSOM_1GB_NAND
# define MTDPARTS_DEFAULT "mtdparts=nand:2048k@0(mlo),2048k@2048k(uboot),512k@4096k(env),5m@4608k(kernel),-@9728k(userfs)"
#endif

#endif /* NAND support */

/*
 * NOR Size = 16 MB
 * No.Of Sectors/Blocks = 128
 * Sector Size = 128 KB
 * Word lenght = 16 bits
 */
#if defined(CONFIG_NOR)
# undef CONFIG_ENV_IS_NOWHERE
# undef CONFIG_SYS_MALLOC_LEN
# define CONFIG_SYS_FLASH_USE_BUFFER_WRITE 1
# define CONFIG_SYS_MALLOC_LEN		(0x100000)
# define CONFIG_SYS_FLASH_CFI
# define CONFIG_FLASH_CFI_DRIVER
# define CONFIG_FLASH_CFI_MTD
# define CONFIG_SYS_MAX_FLASH_SECT	128
# define CONFIG_SYS_MAX_FLASH_BANKS	1
# define CONFIG_SYS_FLASH_BASE		(0x08000000)
# define CONFIG_SYS_MONITOR_BASE	CONFIG_SYS_FLASH_BASE
# define NOR_SECT_SIZE			(128 * 1024)
#if defined (CONFIG_NOR_ENV)
# define CONFIG_ENV_IS_IN_FLASH	1
# define CONFIG_SYS_ENV_SECT_SIZE	(NOR_SECT_SIZE)
# define CONFIG_ENV_SECT_SIZE		(NOR_SECT_SIZE)
# define CONFIG_ENV_OFFSET		(0 * NOR_SECT_SIZE) /* base of NOR */
# define CONFIG_ENV_ADDR		(CONFIG_SYS_FLASH_BASE + \CONFIG_ENV_OFFSET)
#endif /* CONFIG_NOR_ENV */
# define CONFIG_MTD_DEVICE
#endif	/* NOR support */

#if defined(CONFIG_OF_LIBFDT) && defined(CONFIG_OF_SYSTEM_SETUP)
/*
 * FDT System fix-up defines
 */
# define CL_FIXUP_PATH   "/__conf__/"
# define CL_SWAP_TREE_SIZE (0x1 << 14)

/*
 * GPMC fix-up
 */
# define CL_FIXUP_GPMC_UNIT_NAME     "/ocp/gpmc@50000000"
# define CL_FIXUP_GPMC_FIXUP_256MB   (CL_FIXUP_PATH "nand/gpmc@256")
# define CL_FIXUP_GPMC_FIXUP_512MB   (CL_FIXUP_PATH "nand/gpmc@512")
# define CL_FIXUP_GPMC_FIXUP_1024MB  (CL_FIXUP_PATH "nand/gpmc@1024")

/*
 * NOR fix-up
 */
# define CL_FIXUP_NOR_UNIT_NAME        "/ocp/spi@481a0000"
# define CL_FIXUP_NOR_FIXUP_8MB        (CL_FIXUP_PATH "nor/spi@8")
# define CL_FIXUP_NOR_PLACEHOLDER_PATH "/ocp/gpmc@50000000/nand@0,0/partition@0"
#endif /* CONFIG_OF_LIBFDT && CONFIG_OF_SYSTEM_SETUP */

/*
 * Bootcount using the RTC block
 * Need to set bootlimit env variable for it to take action
 * Upon reaching bootlimit altbootcmd env variable will be run instead of bootcmd
 */
#define CONFIG_BOOTCOUNT_LIMIT
#define CONFIG_BOOTCOUNT_AM33XX

/* Enable the HW watchdog, since we can use this with bootcount */
/* Delete these comments to enable watchdog. Make sure kernel is configured to use watchdog first.
#define CONFIG_HW_WATCHDOG
#define CONFIG_OMAP_WATCHDOG
#define CONFIG_SPL_WATCHDOG_SUPPORT
*/

#endif	/* ! __CONFIG_MITYSOM335X_H */
