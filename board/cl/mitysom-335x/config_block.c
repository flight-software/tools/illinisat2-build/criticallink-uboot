/*
 * config_block.c
 *
 *  Created on: Mar 18, 2010
 *      Author: mikew
 */
#include <common.h>
#include <cli.h>
#include <command.h>
#include <image.h>
#include "config_block.h"
#include <spi_flash.h>
#include <asm/arch/hardware.h>
#include <asm/io.h>
#include <asm/setup.h>
#include <errno.h>
#include <i2c.h>

static struct I2CFactoryConfig default_factory_config = {
		.ConfigMagicWord = CONFIG_I2C_MAGIC_WORD,
		.ConfigVersion = CONFIG_I2C_VERSION,
                .MACADDR = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
		.SerialNumber = 120001,
                .FpgaType = 0, /* Leave FpgaType in struct to maintain compatibilty across products */
                .ModelNumber = { '3','3','5','9','-','G','X','-','X','X','6','-','R','C', 0 }
};

struct I2CFactoryConfig __attribute__((section (".data"))) factory_config_block;

int put_factory_config_block(void)
{
	int i, ret;
	u16 sum = 0;
	unsigned char* tmp;
	unsigned int addr = 0x00;

	/* Check if baseboard eeprom is available */
	i2c_set_bus_num(I2C_EEPROM_BUS);
	ret = i2c_probe(I2C_EEPROM_ADDR);
	if (ret) {
		printf
			("Could not probe the EEPROM; something "
			 "fundamentally wrong on the I2C bus.\n");
		return -1;
	}

	tmp = (unsigned char*)&factory_config_block;

	for (i = 0; i < sizeof(factory_config_block); i++)
	{
		sum += *tmp++;
	}

	tmp = (unsigned char*)&factory_config_block;
	ret = 0;
	for (i = 0; i < sizeof(factory_config_block); i++)
	{
		ret |= i2c_write(0x50, addr++, 1, tmp++, 1);
		udelay(11000);
	}

	tmp = (unsigned char*)&sum;
	for (i = 0; i < 2; i++)
	{
		ret |= i2c_write(0x50, addr++, 1, tmp++, 1);
		udelay(11000);
	}

	if (ret) {
		puts("Error Writing I2C Configuration Block\n");
	}
	else {
		puts("I2C Factory Configuration Block Saved\n");
	}
	return ret;
}

/**
 *  return value: -1 i2c access error, 1 bad factory configuratio, 0 = OK
 */
int get_factory_config_block(void)
{
	unsigned char* tmp;
	int i;
	int ret;
	u16 sum, CheckSum;

	/* Check if baseboard eeprom is available */
	i2c_set_bus_num(I2C_EEPROM_BUS);
	ret = i2c_probe(I2C_EEPROM_ADDR);
	if (ret) {
		printf
			("Could not probe the EEPROM; something "
			 "fundamentally wrong on the I2C bus.\n");
		return -1;
	}


	if (i2c_read(0x50, 0x00, 1, (unsigned char*)&factory_config_block, sizeof(factory_config_block)) != 0)
	{
		puts("error reading I2C Configuration Block\n");
		return -1;
	}

	if (i2c_read(0x50, sizeof(factory_config_block), 1, (unsigned char*)&CheckSum, 2) != 0)
	{
		puts("error reading I2C Configuration Block\n");
		return -1;
	}

	/* verify the configuration block is sane */
	tmp = (unsigned char*)&factory_config_block;
	sum = 0;
	for (i = 0; i < sizeof(factory_config_block); i++)
	{
		sum += *tmp++;
	}

	if (sum != CheckSum)
	{
		puts("Error - Factory Configuration Invalid\n");
		puts("You must set the factory configuration to make permanent\n");
		memcpy(&factory_config_block, &default_factory_config, sizeof(factory_config_block));
		return 1;
	}
	return 0;
}

/* This is a trivial atoi implementation since we don't have one available */
int atoi(char *string)
{
        int length;
        int retval = 0;
        int i;
        int sign = 1;

        length = strlen(string);
        for (i = 0; i < length; i++) {
                if (0 == i && string[0] == '-') {
                        sign = -1;
                        continue;
                }
                if (string[i] > '9' || string[i] < '0') {
                        break;
                }
                retval *= 10;
                retval += string[i] - '0';
        }
        retval *= sign;
        return retval;
}

void get_board_serial(struct tag_serialnr *sn)
{
	sn->low = factory_config_block.SerialNumber;
	sn->high = 0;
}

int do_factoryconfig (cmd_tbl_t * cmdtp, int flag, int argc, char * const argv[])
{
	char buffer[80];

	if (argc == 1) {
		/* List configuration info */
			puts ("Factory Configuration:\n");
			printf("Config Version : %d.%d\n",
				factory_config_block.ConfigVersion>>16,
				factory_config_block.ConfigVersion&0xFFFF);
			printf("Serial Number  : %d\n",
				factory_config_block.SerialNumber);
			printf("Model Number    : %s\n",
				factory_config_block.ModelNumber);
	} else {
		unsigned int i;
		if (0 == strncmp(argv[1],"set",3)) {
			sprintf(buffer, "%d", factory_config_block.SerialNumber);
			cli_readline_into_buffer ("Serial Number :", buffer,0);
			i = atoi(buffer);
			if (i > 0) factory_config_block.SerialNumber = i;
			sprintf(buffer, "%s", factory_config_block.ModelNumber);
			cli_readline_into_buffer ("Model Number   :", buffer,0);
			memcpy(factory_config_block.ModelNumber, buffer,
				sizeof(factory_config_block.ModelNumber));
			factory_config_block.ModelNumber
				[sizeof(factory_config_block.ModelNumber)-1] = 0;
		} else if (0 == strncmp(argv[1],"save",4)) {
			put_factory_config_block();
			puts("Configuration Saved\n");
		}
		else {
			puts("Unknown Option\n");
		}
	}

	return 0;
}

/* Only build u-boot commands for u-boot proper, not the SPL */
#ifndef CONFIG_SPL_BUILD

U_BOOT_CMD(factoryconfig, CONFIG_SYS_MAXARGS, 0, do_factoryconfig,
        "mitysom 335x factory config block operations",
	     "    - print current configuration\n"
	"factoryconfig set\n"
	"         - set new configuration\n"
	"factoryconfig save\n"
	"         - write new configuration to I2C FLASH\n"
);

#endif // CONFIG_SPL_BUILD
