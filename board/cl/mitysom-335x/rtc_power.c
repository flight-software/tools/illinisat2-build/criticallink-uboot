/*
 * rtc_power.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */
#include <common.h>
#include <errno.h>
#include <rtc.h>
#include <asm/io.h>
#include <asm/davinci_rtc.h>
#include <asm/arch-am33xx/hardware_am33xx.h>


#ifndef CONFIG_SPL_BUILD

static uchar *rtc_base = (uchar*)RTC_BASE;

#define rtc_read(addr)		__raw_readb(rtc_base + (addr))
#define rtc_write(val, addr)	__raw_writeb(val, rtc_base + (addr))

/* RTC registers */
#define OMAP_RTC_SECONDS_REG		0x00
#define OMAP_RTC_MINUTES_REG		0x04
#define OMAP_RTC_HOURS_REG		0x08
#define OMAP_RTC_DAYS_REG		0x0C
#define OMAP_RTC_MONTHS_REG		0x10
#define OMAP_RTC_YEARS_REG		0x14
#define OMAP_RTC_WEEKS_REG		0x18

#define OMAP_RTC_ALARM_SECONDS_REG	0x20
#define OMAP_RTC_ALARM_MINUTES_REG	0x24
#define OMAP_RTC_ALARM_HOURS_REG	0x28
#define OMAP_RTC_ALARM_DAYS_REG		0x2c
#define OMAP_RTC_ALARM_MONTHS_REG	0x30
#define OMAP_RTC_ALARM_YEARS_REG	0x34

#define OMAP_RTC_CTRL_REG		0x40
#define OMAP_RTC_STATUS_REG		0x44
#define OMAP_RTC_INTERRUPTS_REG		0x48

#define OMAP_RTC_COMP_LSB_REG		0x4c
#define OMAP_RTC_COMP_MSB_REG		0x50
#define OMAP_RTC_OSC_REG		0x54

#define OMAP_RTC_ALARM2_SECONDS_REG	0x80
#define OMAP_RTC_ALARM2_MINUTES_REG	0x84
#define OMAP_RTC_ALARM2_HOURS_REG	0x88
#define OMAP_RTC_ALARM2_DAYS_REG	0x8c
#define OMAP_RTC_ALARM2_MONTHS_REG	0x90
#define OMAP_RTC_ALARM2_YEARS_REG	0x94
#define OMAP_RTC_PMIC_REG		0x98

#define OMAP_RTC_IRQWAKEEN		0x7C

/* OMAP_RTC_CTRL_REG bit fields: */
#define OMAP_RTC_CTRL_SPLIT		(1<<7)
#define OMAP_RTC_CTRL_DISABLE		(1<<6)
#define OMAP_RTC_CTRL_SET_32_COUNTER	(1<<5)
#define OMAP_RTC_CTRL_TEST		(1<<4)
#define OMAP_RTC_CTRL_MODE_12_24	(1<<3)
#define OMAP_RTC_CTRL_AUTO_COMP		(1<<2)
#define OMAP_RTC_CTRL_ROUND_30S		(1<<1)
#define OMAP_RTC_CTRL_STOP		(1<<0)

/* OMAP_RTC_STATUS_REG bit fields: */
#define OMAP_RTC_STATUS_POWER_UP        (1<<7)
#define OMAP_RTC_STATUS_ALARM           (1<<6)
#define OMAP_RTC_STATUS_1D_EVENT        (1<<5)
#define OMAP_RTC_STATUS_1H_EVENT        (1<<4)
#define OMAP_RTC_STATUS_1M_EVENT        (1<<3)
#define OMAP_RTC_STATUS_1S_EVENT        (1<<2)
#define OMAP_RTC_STATUS_RUN             (1<<1)
#define OMAP_RTC_STATUS_BUSY            (1<<0)

/* OMAP_RTC_INTERRUPTS_REG bit fields: */
#define OMAP_RTC_INTERRUPTS_IT_ALARM2	(1<<4)
#define OMAP_RTC_INTERRUPTS_IT_ALARM    (1<<3)
#define OMAP_RTC_INTERRUPTS_IT_TIMER    (1<<2)

/* OMAP_RTC_PMIC_REG bit fields: */
#define OMAP_RTC_PMIC_EXT_WAKEUP_EN_EN (1<<0)
#define OMAP_RTC_PMIC_EXT_WAKEUP_POL (1<<4)
#define OMAP_RTC_PMIC_EXT_WAKEUP_STATUS (1<<12)
#define OMAP_RTC_PMIC_POWER_EN_EN	(1<<16)

#define SHUTDOWN_TIME_SEC		2
#define SECS_IN_MIN			60
#define WAIT_AFTER			(SECS_IN_MIN - SHUTDOWN_TIME_SEC)
#define WAIT_TIME_MS			(SHUTDOWN_TIME_SEC * 1000)

/* OMAP_RTC_IRQWAKEEN bit fields: */
#define OMAP_RTC_IRQWAKEEN_ALARM_WAKEEN    (1<<1)

/*
 * Few RTC IP revision has special WAKEEN Register to enable Wakeup
 * generation for event Alarm.
 */
#define	OMAP_RTC_HAS_IRQWAKEEN		0x2

/* Taken from kernel */

static const unsigned char rtc_days_in_month[] = {
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

static inline bool is_leap_year(unsigned int year)
{
	return (!(year % 4) && (year % 100)) || !(year % 400);
}

/*
 * The number of days in the month.
 */
int rtc_month_days(unsigned int month, unsigned int year)
{
	return rtc_days_in_month[month] + (is_leap_year(year) && month == 1);
}

/*
 * Does the rtc_time represent a valid date/time?
 */
int rtc_valid_tm(struct rtc_time *tm)
{
	if (tm->tm_year < 70
		|| ((unsigned)tm->tm_mon) >= 12
		|| tm->tm_mday < 1
		|| tm->tm_mday > rtc_month_days(tm->tm_mon, tm->tm_year + 1900)
		|| ((unsigned)tm->tm_hour) >= 24
		|| ((unsigned)tm->tm_min) >= 60
		|| ((unsigned)tm->tm_sec) >= 60)
		return -EINVAL;

	return 0;
}

/* we rely on the rtc framework to handle locking (rtc->ops_lock),
 * so the only other requirement is that register accesses which
 * require BUSY to be clear are made with IRQs locally disabled
 */
static void rtc_wait_not_busy(void)
{
	int	count = 0;
	u8	status;

	/* BUSY may stay active for 1/32768 second (~30 usec) */
	for (count = 0; count < 50; count++) {
		status = rtc_read(OMAP_RTC_STATUS_REG);
		if ((status & (u8)OMAP_RTC_STATUS_BUSY) == 0)
			break;
		udelay(1);
	}
	/* now we have ~15 usec to read/write various registers */
}

/* this hardware doesn't support "don't care" alarm fields */
static int tm2bcd(struct rtc_time *tm)
{
	if (rtc_valid_tm(tm) != 0)
		return -EINVAL;

	tm->tm_sec = bin2bcd(tm->tm_sec);
	tm->tm_min = bin2bcd(tm->tm_min);
	tm->tm_hour = bin2bcd(tm->tm_hour);
	tm->tm_mday = bin2bcd(tm->tm_mday);

	tm->tm_mon = bin2bcd(tm->tm_mon + 1);

	/* epoch == 1900 */
	if (tm->tm_year < 100 || tm->tm_year > 199)
		return -EINVAL;
	tm->tm_year = bin2bcd(tm->tm_year - 100);

	return 0;
}

static void bcd2tm(struct rtc_time *tm)
{
	tm->tm_sec = bcd2bin(tm->tm_sec);
	tm->tm_min = bcd2bin(tm->tm_min);
	tm->tm_hour = bcd2bin(tm->tm_hour);
	tm->tm_mday = bcd2bin(tm->tm_mday);
	tm->tm_mon = bcd2bin(tm->tm_mon) - 1;
	/* epoch == 1900 */
	tm->tm_year = bcd2bin(tm->tm_year) + 100;
}


static int omap_rtc_read_time(struct rtc_time *tm)
{
	/* we don't report wday/yday/isdst ... */
	local_irq_disable();
	rtc_wait_not_busy();

	tm->tm_sec = rtc_read(OMAP_RTC_SECONDS_REG);
	tm->tm_min = rtc_read(OMAP_RTC_MINUTES_REG);
	tm->tm_hour = rtc_read(OMAP_RTC_HOURS_REG);
	tm->tm_mday = rtc_read(OMAP_RTC_DAYS_REG);
	tm->tm_mon = rtc_read(OMAP_RTC_MONTHS_REG);
	tm->tm_year = rtc_read(OMAP_RTC_YEARS_REG);

	local_irq_enable();

	bcd2tm(tm);
	return 0;
}


/******************************************************************************
 * Command to switch between NAND HW and SW ecc
 *****************************************************************************/
static int do_rtc_poweroff(cmd_tbl_t *cmdtp, int flag, int argc,
			 char *const argv[])
{
	u32 val;
	struct rtc_time tm;

	/* Clear rtc status */
	writel(0xFFFFFFFF, rtc_base + OMAP_RTC_STATUS_REG);

	/* Set PMIC power enable */
	/* Enable ext wakeup so we can start back up */
	val = readl(rtc_base + OMAP_RTC_PMIC_REG);
	writel(val | OMAP_RTC_PMIC_POWER_EN_EN | OMAP_RTC_PMIC_EXT_WAKEUP_EN_EN,
			rtc_base + OMAP_RTC_PMIC_REG);
	/* TODO Wakeup polarity should be configurable, active high by default */


	/* Wait few seconds instead of rollover */
	do {
		omap_rtc_read_time(&tm);
		if (WAIT_AFTER <= tm.tm_sec)
			mdelay(WAIT_TIME_MS);
	} while (WAIT_AFTER <= tm.tm_sec);

	/* Add shutdown time to the current value */
	tm.tm_sec += SHUTDOWN_TIME_SEC;

	if (tm2bcd(&tm) < 0)
		return -1;

	printf("System will go to power_off state in approx. %d secs\n",
			SHUTDOWN_TIME_SEC);

	/* Set the ALARM2 time */
	rtc_write(tm.tm_sec, OMAP_RTC_ALARM2_SECONDS_REG);
	rtc_write(tm.tm_min, OMAP_RTC_ALARM2_MINUTES_REG);
	rtc_write(tm.tm_hour, OMAP_RTC_ALARM2_HOURS_REG);
	rtc_write(tm.tm_mday, OMAP_RTC_ALARM2_DAYS_REG);
	rtc_write(tm.tm_mon, OMAP_RTC_ALARM2_MONTHS_REG);
	rtc_write(tm.tm_year, OMAP_RTC_ALARM2_YEARS_REG);

	/* Enable alarm2 interrupt */
	val = readl(rtc_base + OMAP_RTC_INTERRUPTS_REG);
	writel(val | OMAP_RTC_INTERRUPTS_IT_ALARM2,
			rtc_base + OMAP_RTC_INTERRUPTS_REG);

	/* Do not allow to execute any other task */
	while (1);

	return 0;
}


U_BOOT_CMD(rtc_poweroff, 1, 1, do_rtc_poweroff,
	"Instruct AM335x RTC to power off system",
	"");
#endif
