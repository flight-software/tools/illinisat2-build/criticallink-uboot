/*
 * mux.c
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */
#include <config.h>
#include "common_def.h"
#include <asm/arch/hardware.h>
#include <asm/arch/mux.h>

#define DEV_ON_BASEBOARD       0
#define DEV_ON_DGHTR_BRD       1

struct evm_pin_mux {
        struct module_pin_mux *mod_pin_mux;
	    unsigned short profile;
		};

static struct module_pin_mux uart0_pin_mux[] = {
	{OFFSET(uart0_rxd), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* UART0_RXD */
	{OFFSET(uart0_txd), (MODE(0) | PULLUDEN)},		/* UART0_TXD */
	{-1},
};

#ifdef CONFIG_NAND
static struct module_pin_mux nand_pin_mux[] = {
	{OFFSET(gpmc_ad0), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD0 */
	{OFFSET(gpmc_ad1), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD1 */
	{OFFSET(gpmc_ad2), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD2 */
	{OFFSET(gpmc_ad3), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD3 */
	{OFFSET(gpmc_ad4), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD4 */
	{OFFSET(gpmc_ad5), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD5 */
	{OFFSET(gpmc_ad6), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD6 */
	{OFFSET(gpmc_ad7), (MODE(0) | PULLUP_EN | RXACTIVE)},	/* NAND AD7 */
	{OFFSET(gpmc_wait0), (MODE(0) | RXACTIVE | PULLUP_EN)}, /* NAND WAIT */
	{OFFSET(gpmc_wpn), (MODE(7) | PULLUP_EN | RXACTIVE)},	/* NAND_WPN */
	{OFFSET(gpmc_csn0), (MODE(0) | PULLUDEN)},	/* NAND_CS0 */
	{OFFSET(gpmc_advn_ale), (MODE(0) | PULLUDEN)}, /* NAND_ADV_ALE */
	{OFFSET(gpmc_oen_ren), (MODE(0) | PULLUDEN)},	/* NAND_OE */
	{OFFSET(gpmc_wen), (MODE(0) | PULLUDEN)},	/* NAND_WEN */
	{OFFSET(gpmc_be0n_cle), (MODE(0) | PULLUDEN)},	/* NAND_BE_CLE */
	{-1},
};
#endif

static struct module_pin_mux i2c0_pin_mux[] = {
	{OFFSET(i2c0_sda), (MODE(0) | RXACTIVE | PULLUDEN | SLEWCTRL)},	/* I2C_DATA */
	{OFFSET(i2c0_scl), (MODE(0) | RXACTIVE | PULLUDEN | SLEWCTRL)},	/* I2C_SCLK */
	{-1},
};

static struct module_pin_mux i2c1_pin_mux[] = {
	{OFFSET(mii1_crs), (MODE(3) | RXACTIVE | PULLUDEN | SLEWCTRL)},	/* I2C_DATA */
	{OFFSET(mii1_rxerr), (MODE(3) | RXACTIVE | PULLUDEN | SLEWCTRL)},	/* I2C_SCLK */
	{-1},
};


static struct module_pin_mux i2c2_pin_mux[] = {
	{OFFSET(uart1_ctsn), (MODE(3) | RXACTIVE | PULLUDEN | SLEWCTRL)},	/* I2C_DATA */
	{OFFSET(uart1_rtsn), (MODE(3) | RXACTIVE | PULLUDEN | SLEWCTRL)},	/* I2C_SCLK */
	{-1},
};

#ifndef CONFIG_NO_ETH

static struct module_pin_mux rgmii1_pin_mux[] = {
	{OFFSET(mii1_txen), MODE(2)},			/* RGMII1_TCTL */
	{OFFSET(mii1_rxdv), MODE(2) | RXACTIVE},	/* RGMII1_RCTL */
	{OFFSET(mii1_txd3), MODE(2)},			/* RGMII1_TD3 */
	{OFFSET(mii1_txd2), MODE(2)},			/* RGMII1_TD2 */
	{OFFSET(mii1_txd1), MODE(2)},			/* RGMII1_TD1 */
	{OFFSET(mii1_txd0), MODE(2)},			/* RGMII1_TD0 */
	{OFFSET(mii1_txclk), MODE(2)},			/* RGMII1_TCLK */
	{OFFSET(mii1_rxclk), MODE(2) | RXACTIVE},	/* RGMII1_RCLK */
	{OFFSET(mii1_rxd3), MODE(2) | RXACTIVE},	/* RGMII1_RD3 */
	{OFFSET(mii1_rxd2), MODE(2) | RXACTIVE},	/* RGMII1_RD2 */
	{OFFSET(mii1_rxd1), MODE(2) | RXACTIVE},	/* RGMII1_RD1 */
	{OFFSET(mii1_rxd0), MODE(2) | RXACTIVE},	/* RGMII1_RD0 */
	{OFFSET(mdio_data), MODE(0) | RXACTIVE | PULLUP_EN}, /* MDIO_DATA */
	{OFFSET(mdio_clk), MODE(0) | PULLUP_EN},	/* MDIO_CLK */
	{-1},
};

static struct module_pin_mux rgmii1_reset_pin_mux[] = {
	{OFFSET(mii1_col), MODE(7)},			/* PHY RESET N gpio3_0 on MITYSOM Maker Board */
	{-1},
};

static struct module_pin_mux rgmii2_pin_mux[] = {
	{OFFSET(gpmc_a0), MODE(2)},			/* RGMII2_TCTL */
	{OFFSET(gpmc_a1), MODE(2) | RXACTIVE},		/* RGMII2_RCTL */
	{OFFSET(gpmc_a2), MODE(2)},			/* RGMII2_TD3 */
	{OFFSET(gpmc_a3), MODE(2)},			/* RGMII2_TD2 */
	{OFFSET(gpmc_a4), MODE(2)},			/* RGMII2_TD1 */
	{OFFSET(gpmc_a5), MODE(2)},			/* RGMII2_TD0 */
	{OFFSET(gpmc_a6), MODE(2)},			/* RGMII2_TCLK */
	{OFFSET(gpmc_a7), MODE(2) | RXACTIVE},		/* RGMII2_RCLK */
	{OFFSET(gpmc_a8), MODE(2) | RXACTIVE},		/* RGMII2_RD3 */
	{OFFSET(gpmc_a9), MODE(2) | RXACTIVE},		/* RGMII2_RD2 */
	{OFFSET(gpmc_a10), MODE(2) | RXACTIVE},		/* RGMII2_RD1 */
	{OFFSET(gpmc_a11), MODE(2) | RXACTIVE},		/* RGMII2_RD0 */
	{OFFSET(mdio_data), MODE(0) | RXACTIVE | PULLUP_EN}, /* MDIO_DATA */
	{OFFSET(mdio_clk), MODE(0) | PULLUP_EN},	/* MDIO_CLK */
        {OFFSET(mii1_rxclk), MODE(7) | PULLUP_EN | PULLUDEN},	/* PHY RESET N on MITYSOM335x devkit */
	{-1},
};

// UNUSED
//static struct module_pin_mux mii1_pin_mux[] = {
//	{OFFSET(mii1_rxerr), MODE(0) | RXACTIVE},	/* MII1_RXERR */
//	{OFFSET(mii1_txen), MODE(0)},			/* MII1_TXEN */
//	{OFFSET(mii1_rxdv), MODE(0) | RXACTIVE},	/* MII1_RXDV */
//	{OFFSET(mii1_txd3), MODE(0)},			/* MII1_TXD3 */
//	{OFFSET(mii1_txd2), MODE(0)},			/* MII1_TXD2 */
//	{OFFSET(mii1_txd1), MODE(0)},			/* MII1_TXD1 */
//	{OFFSET(mii1_txd0), MODE(0)},			/* MII1_TXD0 */
//	{OFFSET(mii1_txclk), MODE(0) | RXACTIVE},	/* MII1_TXCLK */
//	{OFFSET(mii1_rxclk), MODE(0) | RXACTIVE},	/* MII1_RXCLK */
//	{OFFSET(mii1_rxd3), MODE(0) | RXACTIVE},	/* MII1_RXD3 */
//	{OFFSET(mii1_rxd2), MODE(0) | RXACTIVE},	/* MII1_RXD2 */
//	{OFFSET(mii1_rxd1), MODE(0) | RXACTIVE},	/* MII1_RXD1 */
//	{OFFSET(mii1_rxd0), MODE(0) | RXACTIVE},	/* MII1_RXD0 */
//	{OFFSET(mdio_data), MODE(0) | RXACTIVE | PULLUP_EN}, /* MDIO_DATA */
//	{OFFSET(mdio_clk), MODE(0) | PULLUP_EN},	/* MDIO_CLK */
//	{-1},
//};
//
//static struct module_pin_mux rmii1_pin_mux[] = {
//   {OFFSET(mii1_crs), MODE(1) | RXACTIVE},     /* RMII1_CRS */
//   {OFFSET(mii1_rxerr), MODE(1) | RXACTIVE},   /* RMII1_RXERR */
//   {OFFSET(mii1_txen), MODE(1)},           /* RMII1_TXEN */
//   {OFFSET(mii1_txd1), MODE(1)},           /* RMII1_TXD1 */
//   {OFFSET(mii1_txd0), MODE(1)},           /* RMII1_TXD0 */
//   {OFFSET(mii1_rxd1), MODE(1) | RXACTIVE},    /* RMII1_RXD1 */
//   {OFFSET(mii1_rxd0), MODE(1) | RXACTIVE},    /* RMII1_RXD0 */
//   {OFFSET(mdio_data), MODE(0) | RXACTIVE | PULLUP_EN}, /* MDIO_DATA */
//   {OFFSET(mdio_clk), MODE(0) | PULLUP_EN},    /* MDIO_CLK */
//   {OFFSET(rmii1_refclk), MODE(0) | RXACTIVE}, /* RMII1_REFCLK */
//   {-1},
//};
#endif

#ifdef CONFIG_MMC
static struct module_pin_mux mmc0_pin_mux[] = {
	{OFFSET(mmc0_dat3), (MODE(0) | RXACTIVE | PULLUP_EN)},	/* MMC0_DAT3 */
	{OFFSET(mmc0_dat2), (MODE(0) | RXACTIVE | PULLUP_EN)},	/* MMC0_DAT2 */
	{OFFSET(mmc0_dat1), (MODE(0) | RXACTIVE | PULLUP_EN)},	/* MMC0_DAT1 */
	{OFFSET(mmc0_dat0), (MODE(0) | RXACTIVE | PULLUP_EN)},	/* MMC0_DAT0 */
	{OFFSET(mmc0_clk), (MODE(0) | RXACTIVE | PULLUP_EN)},	/* MMC0_CLK */
	{OFFSET(mmc0_cmd), (MODE(0) | RXACTIVE | PULLUP_EN)},	/* MMC0_CMD */
	{-1},
};

static struct module_pin_mux mmc1_pin_mux[] = {
	{OFFSET(gpmc_ad3), (MODE(1) | RXACTIVE | PULLUP_EN)},	/* MMC1_DAT3 */
	{OFFSET(gpmc_ad2), (MODE(1) | RXACTIVE | PULLUP_EN)},	/* MMC1_DAT2 */
	{OFFSET(gpmc_ad1), (MODE(1) | RXACTIVE | PULLUP_EN)},	/* MMC1_DAT1 */
	{OFFSET(gpmc_ad0), (MODE(1) | RXACTIVE | PULLUP_EN)},	/* MMC1_DAT0 */
	{OFFSET(gpmc_csn1), (MODE(2) | RXACTIVE | PULLUP_EN)},	/* MMC1_CLK */
	{OFFSET(gpmc_csn2), (MODE(2) | RXACTIVE | PULLUP_EN)},	/* MMC1_CMD */
	{-1},
};
#endif

#ifdef CONFIG_SPI
static struct module_pin_mux spi0_pin_mux[] = {
	{OFFSET(spi0_sclk), MODE(0) | PULLUDEN | RXACTIVE},	/*SPI0_SCLK */
	{OFFSET(spi0_d0), MODE(0) | PULLUDEN | PULLUP_EN |
							RXACTIVE}, /*SPI0_D0 */
	{OFFSET(spi0_d1), MODE(0) | PULLUDEN |
							RXACTIVE}, /*SPI0_D1 */
	{OFFSET(spi0_cs0), MODE(0) | PULLUDEN | PULLUP_EN | RXACTIVE},	/*SPI0_CS0 */
	{-1},
};

static struct module_pin_mux spi1_pin_mux[] = {
        {OFFSET(ecap0_in_pwm0_out), MODE(4) | PULLUDEN | RXACTIVE},	/*SPI1_SCLK */
        {OFFSET(mcasp0_fsx), MODE(3) | PULLUDEN | PULLUP_EN |
                                                        RXACTIVE}, /*SPI1_D0 */
        {OFFSET(mcasp0_axr0), MODE(3) | PULLUDEN | RXACTIVE}, /*SPI1_D1 */
	{OFFSET(mcasp0_ahclkr), MODE(3) | PULLUDEN | PULLUP_EN |
                                                        RXACTIVE}, /*SPI1_CS0 */
	{-1},
};
#endif

/*
 * Update the structure with the modules present in the general purpose
 * board and the profiles in which the modules are present.
 * If the module is physically present but if it is not available
 * in any of the profile, then do not update it.
 * For eg, nand is avialable only in the profiles 0 and 1, whereas
 * UART0  is available in all the profiles.
 */
static struct evm_pin_mux general_purpose_evm_pin_mux[] = {
	{uart0_pin_mux, PROFILE_ALL},
	{i2c1_pin_mux, PROFILE_ALL},
	{i2c2_pin_mux, PROFILE_ALL},
#ifdef CONFIG_NAND
	{nand_pin_mux, PROFILE_0},
	{nand_pin_mux, PROFILE_1},
#endif
#ifndef CONFIG_NO_ETH
	{rgmii1_pin_mux, PROFILE_2},
	{rgmii1_reset_pin_mux, PROFILE_2},
	{rgmii1_pin_mux, PROFILE_1},
	{rgmii2_pin_mux, PROFILE_0},
#endif

#ifdef CONFIG_MMC
	{mmc0_pin_mux, PROFILE_ALL},
	// eMMC
	{mmc1_pin_mux, PROFILE_2},
#endif
#ifdef CONFIG_SPI
	{spi0_pin_mux, PROFILE_0},
	{spi1_pin_mux, PROFILE_0},
#endif
	{0},
};


static struct evm_pin_mux *am335x_pin_mux[] = {
	general_purpose_evm_pin_mux,
};

/*
 * Check each module in the daughter board(first argument) whether it is
 * available in the selected profile(second argument). If the module is not
 * available in the selected profile, skip the corresponding configuration.
 */
static void set_evm_pin_mux(struct evm_pin_mux *pin_mux,
			int prof)
{
	int i;

	if (!pin_mux)
		return;

	for (i = 0; pin_mux[i].mod_pin_mux != 0; i++)  {
		if ((pin_mux[i].profile & prof) ||
					(prof == PROFILE_NONE)) {
				configure_module_pin_mux(pin_mux[i].
								mod_pin_mux);
		}
	}
}

void configure_evm_pin_mux(unsigned char profile)
{
	set_evm_pin_mux(am335x_pin_mux[0], profile);
}

void enable_mmc0_pin_mux(void)
{
#ifdef CONFIG_MMC
        configure_module_pin_mux(mmc0_pin_mux);
#endif
}
void enable_i2c0_pin_mux(void)
{
        configure_module_pin_mux(i2c0_pin_mux);
}

void enable_i2c1_pin_mux(void)
{
	configure_module_pin_mux(i2c1_pin_mux);
}
void enable_i2c2_pin_mux(void)
{
	configure_module_pin_mux(i2c2_pin_mux);
}

void enable_uart0_pin_mux(void)
{
	configure_module_pin_mux(uart0_pin_mux);
}

