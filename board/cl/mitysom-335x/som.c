/*
 * som.c
 *
 * Copyright (C) 2012 Critical Link LLC - http://www.criticallink.com/
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any
 * kind, whether express or implied; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */
#include <common.h>
#include <errno.h>
#include <spl.h>
#include <asm/arch/cpu.h>
#include <asm/arch/hardware.h>
#include <asm/arch/omap.h>
#include <asm/arch/ddr_defs.h>
#include <asm/arch/clock.h>
#include <asm/arch/gpio.h>
#include <asm/arch/mmc_host_def.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/mem.h>
#include <asm/io.h>
#include <asm/emif.h>
#include <asm/gpio.h>
#include <i2c.h>
#include <miiphy.h>
#include <cpsw.h>
#include <nand.h>
#include <power/tps65910.h>
#include <environment.h>
#include <watchdog.h>
#include "common_def.h"
#include "config_block.h"

/* If FDT system setup functions are enabled */
#if defined(CONFIG_OF_LIBFDT) && defined(CONFIG_OF_SYSTEM_SETUP)
# include <libfdt.h>
# include <fdt_support.h>
#endif

DECLARE_GLOBAL_DATA_PTR;

static struct ctrl_dev *cdev = (struct ctrl_dev *)CTRL_DEVICE_BASE;

/* Profile 0 is the dev kit board, profile 1 is the test fixture, profile 2 is maker board */
#if defined(CONFIG_MITYSOM_MAKER)
static __maybe_unused unsigned char profile = PROFILE_2;
# define BOARD_NAME "Maker Board"
#elif defined(CONFIG_AM335X_TF)
# define BOARD_NAME	"Test Fixture"
static __maybe_unused unsigned char profile = PROFILE_1;
#else
static __maybe_unused unsigned char profile = PROFILE_0;
# define BOARD_NAME "Dev Kit"
#endif

/* #define TRACE_FUNC*/
#ifdef TRACE_FUNC
#ifdef CONFIG_SPL_BUILD
#define tfenter do { \
	printf ("SPL %s:%d %s\n", __FILE__, __LINE__, __func__); } while (0)
#else
#define tfenter do { \
	printf ("%s:%d %s\n", __FILE__, __LINE__, __func__); } while (0)
#endif
#else
#define tfenter
#endif
/* UART Defines */
#define UART_SYSCFG_OFFSET	(0x54)
#define UART_SYSSTS_OFFSET	(0x58)

#define UART_RESET		(0x1 << 1)
#define UART_CLK_RUNNING_MASK	0x1
#define UART_SMART_IDLE_EN	(0x1 << 0x3)

/* Timer Defines */
#define TSICR_REG		0x54
#define TIOCP_CFG_REG		0x10
#define TCLR_REG		0x38

/* DDR defines */
#define MDDR_SEL_DDR2           0xefffffff /* IOs set for DDR2-STL Mode */
#define CKE_NORMAL_OP           0x00000001 /* Normal Op:CKE controlled by EMIF */
#define GATELVL_INIT_MODE_SEL   0x1	/* Selects a starting ratio value based
					   on DATA0/1_REG_PHY_GATELVL_INIT_RATIO_0
					   value programmed by the user */
#define WRLVL_INIT_MODE_SEL     0x1	/* Selects a starting ratio value based
					   on DATA0/1_REG_PHY_WRLVL_INIT_RATIO_0
					   value programmed by the user */

/*
 * I2C Address of various board
 */
#define I2C_TPS65910_CTL_BUS	2 /* On bus 2 except for TiWi module its on 1*/
#define I2C_TPS65910_CTL_BUS_TIWI 1
#define I2C_TPS65910_SMT_BUS	1
static int i2c_tps65910_ctl_bus = I2C_TPS65910_CTL_BUS;

/*
 * Phy Reset
 */
#ifdef CONFIG_MITYSOM_MAKER
/* GPIO 3[0] */
# define GPIO_PHY_RESET_N	96
#else
/* GPIO 3[10] */
# define GPIO_PHY_RESET_N	106
#endif

/* TLK110 PHY registers */
#define TLK110_COARSEGAIN_REG	0x00A3
#define TLK110_LPFHPF_REG	0x00AC
#define TLK110_SPAREANALOG_REG	0x00B9
#define TLK110_VRCR_REG		0x00D0
#define TLK110_SETFFE_REG	(unsigned char)0x0107
#define TLK110_FTSP_REG		(unsigned char)0x0154
#define TLK110_ALFATPIDL_REG	0x002A
#define TLK110_PSCOEF21_REG	0x0096
#define TLK110_PSCOEF3_REG	0x0097
#define TLK110_ALFAFACTOR1_REG	0x002C
#define TLK110_ALFAFACTOR2_REG	0x0023
#define TLK110_CFGPS_REG	0x0095
#define TLK110_FTSPTXGAIN_REG	(unsigned char)0x0150
#define TLK110_SWSCR3_REG	0x000B
#define TLK110_SCFALLBACK_REG	0x0040
#define TLK110_PHYRCR_REG	0x001F

/* TLK110 register writes values */
#define TLK110_COARSEGAIN_VAL	0x0000
#define TLK110_LPFHPF_VAL	0x8000
#define TLK110_SPAREANALOG_VAL	0x0000
#define TLK110_VRCR_VAL		0x0008
#define TLK110_SETFFE_VAL	0x0605
#define TLK110_FTSP_VAL		0x0255
#define TLK110_ALFATPIDL_VAL	0x7998
#define TLK110_PSCOEF21_VAL	0x3A20
#define TLK110_PSCOEF3_VAL	0x003F
#define TLK110_ALFAFACTOR1_VAL	0xFF80
#define TLK110_ALFAFACTOR2_VAL	0x021C
#define TLK110_CFGPS_VAL	0x0000
#define TLK110_FTSPTXGAIN_VAL	0x6A88
#define TLK110_SWSCR3_VAL	0x0000
#define TLK110_SCFALLBACK_VAL	0xC11D
#define TLK110_PHYRCR_VAL	0x4000
#define TLK110_PHYIDR1		0x2000
#define TLK110_PHYIDR2		0xA201

#define NO_OF_MAC_ADDR          3
#define ETH_ALEN		6

#ifdef CONFIG_SPL_BUILD
#if defined(CONFIG_SYS_NAND_PAGE_SIZE)
static const int nand_page_size = CONFIG_SYS_NAND_PAGE_SIZE;
#else
static const int nand_page_size = 0;
#endif

static __attribute__((section (".data"))) ulong ram_size = 0;

#ifdef CONFIG_SPL_OS_BOOT
int spl_start_uboot(void)
{
	tfenter;
	/* break into full u-boot on 'c' */
	return (serial_tstc() && serial_getc() == 'c');
}
#endif

#define CONFIG_SYS_ALT_MEMTEST
#if !defined(CONFIG_SYS_ALT_MEMTEST)
/*
 * Perform a memory test. A more complete alternative test can be
 * configured using CONFIG_SYS_ALT_MEMTEST. The complete test loops until
 * interrupted by ctrl-c or by a failure of one of the sub-tests.
 */
static int mtest(ulong *apStart, ulong *apEnd)
{
	vu_long *addr, *start, *end;
	ulong val;
	ulong readback;
	ulong errs = 0;
	ulong err_limit = 8;	/* Really 1, but show up to 32 */
	int iterations = 1;
	int iteration_limit = 2;

	ulong incr;
	ulong pattern = 0xA5A5A5A5;

	tfenter;

	if (apStart)
		start = apStart;
	else
		start = (ulong *) CONFIG_SYS_MEMTEST_START;

	if (apEnd)
		end = apEnd;
	else
		end = (ulong *) (CONFIG_SYS_MEMTEST_END);

	incr = 1;
	printf("Testing RAM from %08x to %08x\n", (unsigned int)start,
	       (unsigned int)end);
	for (;;) {
		if (iteration_limit && (iterations > iteration_limit)) {
			printf("Tested %d iteration(s) with %lu errors.\n",
			       iterations - 1, errs);
			return errs != 0;
		}
		++iterations;
		if (ctrlc())
			return -1;

		printf("\rPattern %08lX  Writing..."
		       "%12s" "\b\b\b\b\b\b\b\b\b\b", pattern, "");

		for (addr = start, val = pattern; addr < end; addr++)
			*addr = val;

		printf("Done\nValidating...");

		errs = 0;
		for (addr = start, val = pattern;
		     (errs < err_limit) && (addr < end); addr++) {
			readback = *addr;
			if (readback != val) {
				printf("Mem error @ 0x%08X: "
				       "found %08lX, expected %08lX\n",
				       (uint) (uintptr_t) addr, readback, val);
				errs++;
			}
		}
		if (errs > err_limit) {
			printf("Error limit[%d] exceeded!\n", (int)err_limit);
			return 1;
		}

		/*
		 * Flip the pattern each time to make lots of zeros and
		 * then, the next time, lots of ones.  We decrement
		 * the "negative" patterns and increment the "positive"
		 * patterns to preserve this feature.
		 */
		if (pattern & 0x80000000)
			pattern = -pattern;	/* complement & increment */
		else
			pattern = ~pattern;
		incr = -incr;
	}
	return 0;
}

#else

static void print_high_low(ulong expected, ulong actual)
{
	if ((expected & 0xFF00FF00) != (actual & 0xFF00FF00))
	{
		printf(" (high byte)");
	}
	if ((expected & 0x00FF00FF) != (actual & 0x00FF00FF))
	{
		printf(" (low byte)");
	}
}

#include <usb.h> // For __swap_32
/* Copied from cmd_mem.c */
static ulong mem_test_alt(ulong start_addr, ulong end_addr)
{
	vu_long *addr = (void *)(uintptr_t)start_addr;
	vu_long *dummy;
	ulong cur_addr;
	ulong errs = 0;
	ulong err_prints = 0;
	ulong err_limit = 8;
	ulong val, readback;
	int j;
	vu_long offset;
	vu_long test_offset;
	vu_long pattern;
	vu_long temp;
	vu_long anti_pattern;
	vu_long num_words;
	static const ulong bitpattern[] = {
		0x00000001,	/* single bit */
		0x00000003,	/* two adjacent bits */
		0x00000007,	/* three adjacent bits */
		0x0000000F,	/* four adjacent bits */
		0x00000005,	/* two non-adjacent bits */
		0x00000015,	/* three non-adjacent bits */
		0x00000055,	/* four non-adjacent bits */
		0xaaaaaaaa,	/* alternating 1/0 */
	};
	int iterations = 1;
	int iteration_limit = 1;
	int count;

	tfenter;

	for (;;) {
		if (iteration_limit && (iterations > iteration_limit)) {
			printf("Tested %d iteration(s) with %lu errors.\n",
					iterations - 1, errs);
			return errs != 0;
		}
		++iterations;

		if (ctrlc())
			return -1;

		num_words = (end_addr - start_addr) / sizeof(vu_long);

		printf("Testing RAM from %08x to %08x. Total words: %lu\n", (unsigned int)start_addr,
				(unsigned int)end_addr, num_words);

		/*
		 * Data line test: write a pattern to the first
		 * location, write the 1's complement to a 'parking'
		 * address (changes the state of the data bus so a
		 * floating bus doesn't give a false OK), and then
		 * read the value back. Note that we read it back
		 * into a variable because the next time we read it,
		 * it might be right (been there, tough to explain to
		 * the quality guys why it prints a failure when the
		 * "is" and "should be" are obviously the same in the
		 * error message).
		 *
		 * Rather than exhaustively testing, we test some
		 * patterns by shifting '1' bits through a field of
		 * '0's and '0' bits through a field of '1's (i.e.
		 * pattern and ~pattern).
		 */
		printf("Data line test...");
		addr = (void *)(uintptr_t)start_addr;
		dummy = addr+sizeof(addr);
		err_prints = 0;
		//Loop x times to ensure test takes close to a second
		for (count = 0; count < 500; count++) {
			for (j = 0; j < sizeof(bitpattern) / sizeof(bitpattern[0]); j++) {
				val = bitpattern[j];
				for (; val != 0; val <<= 1) {
					*addr = val;
					*dummy  = ~val; /* clear the test data off the bus */
					readback = *addr;
					if (readback != val) {
						if (err_prints > err_limit) {
							break;
						}
						printf("\nFAILURE %04d (data line): "
							"expected 0x%.8lx, actual 0x%.8lx",
								count, val, readback);
						print_high_low(val, readback);
						errs++;
						err_prints++;
						if (ctrlc())
							return -1;
					}
					*addr  = ~val;
					*dummy  = val;
					readback = *addr;
					if (readback != ~val) {
						printf("\nFAILURE %04d (data line): "
							"expected 0x%.8lx, actual 0x%.8lx",
								count, ~val, readback);
						print_high_low(~val, readback);
						errs++;
						if (ctrlc())
							return -1;
					}
				}
			}
		}

		/*
		 * Based on code whose Original Author and Copyright
		 * information follows: Copyright (c) 1998 by Michael
		 * Barr. This software is placed into the public
		 * domain and may be used for any purpose. However,
		 * this notice must not be changed or removed and no
		 * warranty is either expressed or implied by its
		 * publication or distribution.
		 */

		/*
		* Address line test

		 * Description: Test the address bus wiring in a
		 *              memory region by performing a walking
		 *              1's test on the relevant bits of the
		 *              address and checking for aliasing.
		 *              This test will find single-bit
		 *              address failures such as stuck-high,
		 *              stuck-low, and shorted pins. The base
		 *              address and size of the region are
		 *              selected by the caller.

		 * Notes:	For best results, the selected base
		 *              address should have enough LSB 0's to
		 *              guarantee single address bit changes.
		 *              For example, to test a 64-Kbyte
		 *              region, select a base address on a
		 *              64-Kbyte boundary. Also, select the
		 *              region size as a power-of-two if at
		 *              all possible.
		 *
		 * Returns:     0 if the test succeeds, 1 if the test fails.
		 */
		pattern = (vu_long) 0xaaaaaaaa;
		anti_pattern = (vu_long) 0x55555555;

		/* Test all address lines */
		addr = (void *)(uintptr_t)CONFIG_SYS_SDRAM_BASE;
		num_words = ram_size / sizeof(vu_long);

		//Loop x times to ensure test takes close to a second
		err_prints = 0;
		for (count = 0; count < 500; count++) {
			if (count == 0) {
				debug("%s:%d: length = 0x%.8lx\n", __func__, __LINE__, num_words);
				printf("Addr line test...writing...");
			}
			/*
			 * Write the default pattern at each of the
			 * power-of-two offsets.
			 */
			for (offset = 1; offset < num_words; offset <<= 1)
				addr[offset] = pattern;

			/*
			 * Check for address bits stuck high.
			 */
			test_offset = 0;
			addr[test_offset] = anti_pattern;

			if (count == 0) {
				printf("testing stuck high...");
			}
			for (offset = 1; offset < num_words; offset <<= 1) {
				temp = addr[offset];
				if (temp != pattern) {
					if (err_prints > err_limit) {
						break;
					}
					printf("\nFAILURE %04d: Address bit stuck high @ 0x%.8lx:"
						" expected 0x%.8lx, actual 0x%.8lx",
						count, start_addr + offset*sizeof(vu_long), pattern, temp);
					print_high_low(pattern, temp);
					errs++;
					err_prints++;
					if (ctrlc())
						return -1;
				}
			}
			addr[test_offset] = pattern;

			/*
			 * Check for addr bits stuck low or shorted.
			 */
			if (count == 0) {
				printf("testing stuck low...");
			}
			for (test_offset = 1; test_offset < num_words; test_offset <<= 1) {
				addr[test_offset] = anti_pattern;

				for (offset = 1; offset < num_words; offset <<= 1) {
					temp = addr[offset];
					if ((temp != pattern) && (offset != test_offset)) {
						if (err_prints > err_limit) {
							break;
						}
						printf("\nFAILURE %04d: Address bit stuck low or"
							" shorted @ 0x%.8lx: expected 0x%.8lx,"
							" actual 0x%.8lx, test_offset 0x%.8lx",
							count,
							start_addr + offset*sizeof(vu_long),
							pattern, temp, test_offset);
						print_high_low(pattern, temp);
						errs++;
						err_prints++;
						if (ctrlc())
							return -1;
					}
				}
				addr[test_offset] = pattern;
			}
		}
		printf("done\n");

		/*
		 * Description: Test the integrity of a physical
		 *		memory device by performing an
		 *		increment/decrement test over the
		 *		entire region. In the process every
		 *		storage bit in the device is tested
		 *		as a zero and a one. The base address
		 *		and the size of the region are
		 *		selected by the caller.
		 *
		 * Returns:     0 if the test succeeds, 1 if the test fails.
		 */

		/*
		 * Fill memory with a known count pattern.
		 */
		addr = (void *)(uintptr_t)start_addr;
		num_words = (end_addr - start_addr) / sizeof(vu_long);
		printf("Memtest...writing...");
		for (offset = 0; offset < num_words; offset++) {
			// Use pointer address as count pattern
			addr[offset] = (vu_long)&addr[offset];
		}

		// Break memtest into 16M increments
		const long ADDR_INCREMENT = 16*1024*1024;
		for(cur_addr = start_addr; cur_addr < end_addr; cur_addr += ADDR_INCREMENT) {
			addr = (void *)(uintptr_t)cur_addr;
			num_words = min((end_addr - cur_addr), ADDR_INCREMENT) / sizeof(vu_long);

			/*
			 * Check each location and invert it for the second pass.
			 */
			printf("\nreading @ 0x%.8lx words:%lu...", cur_addr, num_words);
			err_prints = 0;
			for (offset = 0; offset < num_words; offset++) {
				temp = addr[offset];
				pattern = (vu_long)&addr[offset];
				if (temp != pattern) {
					if (err_prints > err_limit) {
						printf("\n");
						break;
					}
					printf("\nFAILURE: (read/write) @ 0x%.8lx:"
						" expected 0x%.8lx, actual 0x%.8lx",
						(vu_long)&addr[offset], pattern, temp);
					print_high_low(pattern, temp);
					err_prints++;
					errs++;
					if (ctrlc())
						return -1;
				}
			}
		}

		/*
		 * Fill inverse pattern
		 */
		addr = (void *)(uintptr_t)start_addr;
		num_words = (end_addr - start_addr) / sizeof(vu_long);
		printf("\ninverting...");
		for (offset = 0; offset < num_words; offset++) {
			// Use pointer address as count pattern
			addr[offset] = ~(vu_long)&addr[offset];
		}

		// Break memtest into 16M increments
		for(cur_addr = start_addr; cur_addr < end_addr; cur_addr += ADDR_INCREMENT) {
			addr = (void *)(uintptr_t)cur_addr;
			num_words = min((end_addr - cur_addr), ADDR_INCREMENT) / sizeof(vu_long);

			/*
			 * Check each location for the inverted pattern and zero it.
			 */
			printf("\nreading @ 0x%.8lx words:%lu...", cur_addr, num_words);
			err_prints = 0;
			for (offset = 0; offset < num_words; offset++) {
				pattern = (vu_long)&addr[offset];
				anti_pattern = ~pattern;
				temp = addr[offset];
				if (temp != anti_pattern) {
					if (err_prints > err_limit) {
						printf("\n");
						break;
					}
					printf("\nFAILURE: (read/write): @ 0x%.8lx:"
						" expected 0x%.8lx, actual 0x%.8lx",
						(vu_long)&addr[offset], anti_pattern, temp);
					print_high_low(anti_pattern, temp);
					err_prints++;
					errs++;
					if (ctrlc())
						return -1;
				}
			}
		}

		/*
		 * Fill memory with a known count pattern.
		 * Byte swapped 0x12345678 -> 0x78453412
		 * Swap bytes incase only the low byte ram u9 is faulty, we catch failures in BA pins
		 */
		addr = (void *)(uintptr_t)start_addr;
		num_words = (end_addr - start_addr) / sizeof(vu_long);
		printf("\nMemtest_swapped...writing...");
		for (offset = 0; offset < num_words; offset++) {
			// Use pointer address as count pattern
			addr[offset] = __swap_32((vu_long)&addr[offset]);
		}

		// Break memtest into 16M increments
		for(cur_addr = start_addr; cur_addr < end_addr; cur_addr += ADDR_INCREMENT) {
			addr = (void *)(uintptr_t)cur_addr;
			num_words = min((end_addr - cur_addr), ADDR_INCREMENT) / sizeof(vu_long);
			/*
			 * Check each location and invert it for the second pass.
			 */
			printf("\nreading @ 0x%.8lx words:%lu...", cur_addr, num_words);
			err_prints = 0;
			for (offset = 0; offset < num_words; offset++) {
				temp = addr[offset];
				pattern = __swap_32((vu_long)&addr[offset]);
				if (temp != pattern) {
					if (err_prints > err_limit) {
						printf("\n");
						break;
					}
					printf("\nFAILURE: (read/write) @ 0x%.8lx:"
						" expected 0x%.8lx, actual 0x%.8lx)",
						(vu_long)&addr[offset], pattern, temp);
					print_high_low(pattern, temp);
					err_prints++;
					errs++;
					if (ctrlc())
						return -1;
				}
			}
		}

		/*
		 * Fill inverse pattern
		 * Byte swapped 0x12345678 -> 0x78453412
		 */
		addr = (void *)(uintptr_t)start_addr;
		num_words = (end_addr - start_addr) / sizeof(vu_long);
		printf("\ninverting...");
		for (offset = 0; offset < num_words; offset++) {
			// Use pointer address as count pattern
			addr[offset] = __swap_32(~(vu_long)&addr[offset]);
		}

		// Break memtest into 16M increments
		for(cur_addr = start_addr; cur_addr < end_addr; cur_addr += ADDR_INCREMENT) {
			addr = (void *)(uintptr_t)cur_addr;
			num_words = min((end_addr - cur_addr), ADDR_INCREMENT) / sizeof(vu_long);

			/*
			 * Check each location for the inverted pattern and zero it.
			 */
			printf("\nreading @ 0x%.8lx words:%lu...", cur_addr, num_words);
			err_prints = 0;
			for (offset = 0; offset < num_words; offset++) {
				pattern = __swap_32((vu_long)&addr[offset]);
				anti_pattern = ~pattern;
				temp = addr[offset];
				if (temp != anti_pattern) {
					if (err_prints > err_limit) {
						printf("\n");
						break;
					}
					printf("\nFAILURE: (read/write): @ 0x%.8lx:"
						" expected 0x%.8lx, actual 0x%.8lx)",
						(vu_long)&addr[offset], anti_pattern, temp);
					print_high_low(anti_pattern, temp);
					err_prints++;
					errs++;
					if (ctrlc())
						return -1;
				}
			}
		}
		printf("done\n");
	}

	return 0;
}

#endif /* CONFIG_SYS_ALT_MEMTEST */

#endif /* CONFIG_SPL_BUILD */

/* set the D3 LED to on of off. This LED is hooked up to the GPIO0
 * output of the PMIC.
 * \param[in] b if non-zero, turn LED on, else off
 */
void set_led_d3(int b)
{
	unsigned char v;
	/* save the bus number */
	int bus = i2c_get_bus_num();

	tfenter;

	if(bus != i2c_tps65910_ctl_bus)
		i2c_set_bus_num(i2c_tps65910_ctl_bus);

	i2c_read(TPS65910_CTRL_I2C_ADDR, TPS65910_GPIO0_REG, 1, &v, 1);
	/* Ensure GPIO pin set as output */
	v |= TPS65910_GPIO0_REG_GPIO_CFG;
	/* Ensure GPIO pull-up is disabled, pull-up exists on board */
	v &= ~TPS65910_GPIO0_REG_GPIO_PUEN;

	if (b)
		v |= TPS65910_GPIO0_REG_GPIO_SET;
	else
		v &= ~TPS65910_GPIO0_REG_GPIO_SET;

	i2c_write(TPS65910_CTRL_I2C_ADDR, TPS65910_GPIO0_REG, 1, &v, 1);

	/* put it back */
	if(bus != i2c_tps65910_ctl_bus)
		i2c_set_bus_num(bus);
}

#ifdef CONFIG_SPL_BUILD

static const struct ddr_data ddr2_data = {
        .datardsratio0 = MT47H128M16RT25E_RD_DQS,
        .datafwsratio0 = MT47H128M16RT25E_PHY_FIFO_WE,
        .datawrsratio0 = MT47H128M16RT25E_PHY_WR_DATA,
};

static const struct cmd_control ddr2_cmd_ctrl_data = {
	.cmd0csratio = MT47H128M16RT25E_RATIO,

	.cmd1csratio = MT47H128M16RT25E_RATIO,

	.cmd2csratio = MT47H128M16RT25E_RATIO,
};

static const struct emif_regs ddr2_emif_reg_data = {
	.sdram_config = MT47H128M16RT25E_EMIF_SDCFG,
	.ref_ctrl = MT47H128M16RT25E_EMIF_SDREF,
	.sdram_tim1 = MT47H128M16RT25E_EMIF_TIM1,
	.sdram_tim2 = MT47H128M16RT25E_EMIF_TIM2,
	.sdram_tim3 = MT47H128M16RT25E_EMIF_TIM3,
	.ocp_config = EMIF_OCP_CONFIG_AM335X_MITYSOM,
	.emif_ddr_phy_ctlr_1 = MT47H128M16RT25E_EMIF_READ_LATENCY,
};

static const struct ddr_data ddr3_256m_data = {
	.datardsratio0 = MT41K128M8JP15E_RD_DQS,
	.datawdsratio0 = MT41K128M8JP15E_WR_DQS,
	.datafwsratio0 = MT41K128M8JP15E_PHY_FIFO_WE,
	.datawrsratio0 = MT41K128M8JP15E_PHY_WR_DATA,
};

static const struct cmd_control ddr3_256m_cmd_ctrl_data = {
	.cmd0csratio = MT41K128M8JP15E_RATIO,
	.cmd0iclkout = MT41K128M8JP15E_INVERT_CLKOUT,

	.cmd1csratio = MT41K128M8JP15E_RATIO,
	.cmd1iclkout = MT41K128M8JP15E_INVERT_CLKOUT,

	.cmd2csratio = MT41K128M8JP15E_RATIO,
	.cmd2iclkout = MT41K128M8JP15E_INVERT_CLKOUT,
};

static struct emif_regs ddr3_256m_emif_reg_data = {
	.sdram_config = MT41K128M8JP15E_EMIF_SDCFG,
	.ref_ctrl = MT41K128M8JP15E_EMIF_SDREF,
	.sdram_tim1 = MT41K128M8JP15E_EMIF_TIM1,
	.sdram_tim2 = MT41K128M8JP15E_EMIF_TIM2,
	.sdram_tim3 = MT41K128M8JP15E_EMIF_TIM3,
	.zq_config = MT41K128M8JP15E_ZQ_CFG,
	.ocp_config = EMIF_OCP_CONFIG_AM335X_MITYSOM,
	.emif_ddr_phy_ctlr_1 = MT41K128M8JP15E_EMIF_READ_LATENCY |
				PHY_EN_DYN_PWRDN,
};

static const struct ddr_data ddr3_512m_data = {
	.datardsratio0 = MT41K256M8DA125_RD_DQS,
	.datawdsratio0 = MT41K256M8DA125_WR_DQS,
	.datafwsratio0 = MT41K256M8DA125_PHY_FIFO_WE,
	.datawrsratio0 = MT41K256M8DA125_PHY_WR_DATA,
};

static const struct cmd_control ddr3_512m_cmd_ctrl_data = {
	.cmd0csratio = MT41K256M8DA125_RATIO,
	.cmd0iclkout = MT41K256M8DA125_INVERT_CLKOUT,

	.cmd1csratio = MT41K256M8DA125_RATIO,
	.cmd1iclkout = MT41K256M8DA125_INVERT_CLKOUT,

	.cmd2csratio = MT41K256M8DA125_RATIO,
	.cmd2iclkout = MT41K256M8DA125_INVERT_CLKOUT,
};

static struct emif_regs ddr3_512m_emif_reg_data = {
	.sdram_config = MT41K256M8DA125_EMIF_SDCFG,
	.ref_ctrl = MT41K256M8DA125_EMIF_SDREF,
	.sdram_tim1 = MT41K256M8DA125_EMIF_TIM1,
	.sdram_tim2 = MT41K256M8DA125_EMIF_TIM2,
	.sdram_tim3 = MT41K256M8DA125_EMIF_TIM3,
	.zq_config = MT41K256M8DA125_ZQ_CFG,
	.ocp_config = EMIF_OCP_CONFIG_AM335X_MITYSOM,
	.emif_ddr_phy_ctlr_1 = MT41K256M8DA125_EMIF_READ_LATENCY |
				PHY_EN_DYN_PWRDN,
};

static const struct ddr_data ddr3_1024m_data = {
	.datardsratio0 = MT41K512M8RH125_RD_DQS,
	.datawdsratio0 = MT41K512M8RH125_WR_DQS,
	.datafwsratio0 = MT41K512M8RH125_PHY_FIFO_WE,
	.datawrsratio0 = MT41K512M8RH125_PHY_WR_DATA,
};

static const struct cmd_control ddr3_1024m_cmd_ctrl_data = {
	.cmd0csratio = MT41K512M8RH125_RATIO,
	.cmd0iclkout = MT41K512M8RH125_INVERT_CLKOUT,

	.cmd1csratio = MT41K512M8RH125_RATIO,
	.cmd1iclkout = MT41K512M8RH125_INVERT_CLKOUT,

	.cmd2csratio = MT41K512M8RH125_RATIO,
	.cmd2iclkout = MT41K512M8RH125_INVERT_CLKOUT,
};

static struct emif_regs ddr3_1024m_emif_reg_data = {
	.sdram_config = MT41K512M8RH125_EMIF_SDCFG,
	.ref_ctrl = MT41K512M8RH125_EMIF_SDREF,
	.sdram_tim1 = MT41K512M8RH125_EMIF_TIM1,
	.sdram_tim2 = MT41K512M8RH125_EMIF_TIM2,
	.sdram_tim3 = MT41K512M8RH125_EMIF_TIM3,
	.zq_config = MT41K512M8RH125_ZQ_CFG,
	.ocp_config = EMIF_OCP_CONFIG_AM335X_MITYSOM,
	.emif_ddr_phy_ctlr_1 = MT41K512M8RH125_EMIF_READ_LATENCY |
				PHY_EN_DYN_PWRDN,
};

/**
 * MitySOM335X has a couple of DDR3 configuraiton options
 * Config
 * 0	- 256 MB ->  2 x MT41K128M8JP-125:G
 *                   2 x [128M x 8] = 256MB DDR3 RAM
 * 1	- 512 MB ->  2 x MT41K256M8DA-125:M
 *                   2 x [256M x 8] = 512MB DDR3 RAM
 * 2	- 1024 MB -> 2 x MT41K512M8RH-125:M
 *                   2 x [512M x 8] = 1024MB DDR3 RAM
 *
 * MT41K128M8JP
 * - 14  Addr lines            A[13:0]
 * - 3   bank address lines    BA[2:0]
 * - 10  Column address lines  A[9:0]
 * - CAS Latency = 5
 * - CWL Latency = 5
 *
 * MT41K256M8DA
 * - 15  Addr lines            A[14:0]
 * - 3   bank address lines    BA[2:0]
 * - 10  Column address lines  A[9:0]
 * - CAS Latency = 5
 * - CWL Latency = 5
 *
 */

const struct ctrl_ioregs ioregs_512MB = {
	.cm0ioctl		= MT41K256M8DA125_IOCTRL_VALUE,
	.cm1ioctl		= MT41K256M8DA125_IOCTRL_VALUE,
	.cm2ioctl		= MT41K256M8DA125_IOCTRL_VALUE,
	.dt0ioctl		= MT41K256M8DA125_IOCTRL_VALUE,
	.dt1ioctl		= MT41K256M8DA125_IOCTRL_VALUE,
};

const struct ctrl_ioregs ioregs_256MB = {
        .cm0ioctl               = MT41K128M8JP15E_IOCTRL_VALUE,
        .cm1ioctl               = MT41K128M8JP15E_IOCTRL_VALUE,
        .cm2ioctl               = MT41K128M8JP15E_IOCTRL_VALUE,
        .dt0ioctl               = MT41K128M8JP15E_IOCTRL_VALUE,
        .dt1ioctl               = MT41K128M8JP15E_IOCTRL_VALUE,
};

const struct ctrl_ioregs ioregs_1GB = {
        .cm0ioctl               = MT41K512M8RH125_IOCTRL_VALUE,
        .cm1ioctl               = MT41K512M8RH125_IOCTRL_VALUE,
        .cm2ioctl               = MT41K512M8RH125_IOCTRL_VALUE,
        .dt0ioctl               = MT41K512M8RH125_IOCTRL_VALUE,
        .dt1ioctl               = MT41K512M8RH125_IOCTRL_VALUE,
};



static void config_am335x_ddr3(int bus_speed, int config)
{
	tfenter;

	switch (config) {
	case RAM_SIZE_512MB_DDR3:
		{
			config_ddr(bus_speed, &ioregs_512MB,
				   &ddr3_512m_data, &ddr3_512m_cmd_ctrl_data,
				   &ddr3_512m_emif_reg_data, 0);
			ram_size = 512 * 1024 * 1024;
		}
		break;
	case RAM_SIZE_256MB_DDR3:
		{
			config_ddr(bus_speed, &ioregs_256MB,
				   &ddr3_256m_data, &ddr3_256m_cmd_ctrl_data,
				   &ddr3_256m_emif_reg_data, 0);
			ram_size = 256 * 1024 * 1024;
		}
		break;
	case RAM_SIZE_1GB_DDR3:
		{
			config_ddr(bus_speed, &ioregs_1GB,
				   &ddr3_1024m_data, &ddr3_1024m_cmd_ctrl_data,
				   &ddr3_1024m_emif_reg_data, 0);
			ram_size = 1024 * 1024 * 1024;
		}
		break;
	default:
		printf("%s:%d  %s Unknown RAM configuration %d\n", __FILE__,
		       __LINE__, __func__, config);
		break;
	}
}

/**
 * MitySOM335X uses the Micron Mt47H128M16RT-25E for DDR2
 * 16M x 16 x 8 bank [128M x 16 = 256MB] DDR2 RAM
 * This part has:
 * 14 Addr lines            A[13:0]
 * 3 bank address lines    BA[2:0]
 * 10 Column address lines  A[9:0]
 * CaS Latency = 5
 */

const struct ctrl_ioregs ioregs_ddr2 = {
	.cm0ioctl		= MT47H128M16RT25E_IOCTRL_VALUE,
	.cm1ioctl		= MT47H128M16RT25E_IOCTRL_VALUE,
	.cm2ioctl		= MT47H128M16RT25E_IOCTRL_VALUE,
	.dt0ioctl		= MT47H128M16RT25E_IOCTRL_VALUE,
	.dt1ioctl		= MT47H128M16RT25E_IOCTRL_VALUE, 
};

static void config_am335x_ddr2(int bus_speed)
{
	tfenter;

	config_ddr(bus_speed, &ioregs_ddr2, &ddr2_data,
		   &ddr2_cmd_ctrl_data, &ddr2_emif_reg_data, 0);
	ram_size = 256 * 1024 * 1024;
}

#endif /* CONFIG_SPL_BUILD */

/**
 * Determine TPS65910 CTRL bus. Bus was moved to i2c1 on the TIWI module.  Its
 * i2c2 on all other modules.
 */
void configure_tps65910_ctrl_bus(void)
{
	tfenter;

	// Check for tiwi module ending, -ble or -r2
	if(IS_TIWI_MODULE)
	{
		i2c_tps65910_ctl_bus = I2C_TPS65910_CTL_BUS_TIWI;
	}
	else
	{
		i2c_tps65910_ctl_bus = I2C_TPS65910_CTL_BUS;
	}
}

/**
 * Check model number to make sure u-boot
 * is configured correctly. 
 */
void check_nand_support(void)
{
	tfenter;

#ifndef CONFIG_MITYSOM_1GB_NAND
	if(factory_config_block.ModelNumber[NAND_SIZE_POSITION] == NAND_SIZE_1GB)
		printf("Critical Link: Error u-boot not compiled with for 1GB nand support. Nand may not function correctly.\n");
#endif
	/* 512MB Nand requires specific changes enabled by CONFIG_MITYSOM_512MB_NAND.  */
#ifndef CONFIG_MITYSOM_512MB_NAND
	if(factory_config_block.ModelNumber[NAND_SIZE_POSITION] == NAND_SIZE_512MB)
		printf("Critical Link: Error u-boot not compiled with for 512MB nand support. Nand may not function correctly.\n");
#endif
	/* 256MB Nand requires 2K Page setup */
#ifndef CONFIG_MITYSOM_256MB_NAND
	if(factory_config_block.ModelNumber[NAND_SIZE_POSITION] == NAND_SIZE_256MB)
		printf("Critical Link: Error u-boot compiled for large page nand support. Nand may not function correctly.\n");
#endif
}

int set_fdt_config(void)
{
	/* Set "nand_size" var */
	switch(factory_config_block.ModelNumber[NAND_SIZE_POSITION])
	{
		case NAND_SIZE_256MB:
			env_set("nand_size", "nand256");
			break;
		case NAND_SIZE_512MB:
			env_set("nand_size", "nand512");
			break;
		case NAND_SIZE_1GB:
			env_set("nand_size", "nand1024");
			break;
	}

	/* Set "nor_size" var */
	switch(factory_config_block.ModelNumber[NOR_SIZE_POSITION])
	{
		case NOR_SIZE_8MB:
			env_set("nor_size", "nor8");
			break;
	}

	/* Set "board_type" var */
	if (!strcmp("Maker Board",BOARD_NAME))
	{
		env_set("board_type", "makerbrd");
	}
	else if (!strcmp("Test Fixture",BOARD_NAME))
	{
		env_set("board_type", "testfix");
	}
	else if (!strcmp("Dev Kit",BOARD_NAME))
	{
		env_set("board_type", "devkit");
	}
	return 0;
}

#ifndef NUM_EEPROM_RETRIES
#define NUM_EEPROM_RETRIES	2
#endif

/*
 * Read header information from EEPROM into global structure.
 */
int read_eeprom(void)
{
	tfenter;

	int retries, rv;

	for (retries = 0; retries <= NUM_EEPROM_RETRIES; ++retries) {
		if (retries)
			printf("Retrying [%d] ...\n", retries);

		/* try and read our configuration block */
		rv = get_factory_config_block();
		if (rv < 0) {
			printf("I2C Error reading factory config block\n");
			continue;	/* retry */
		}
		/* No I2C issues, data was either OK or not entered... */
		if (rv > 0)
			printf("Bad I2C configuration found\n");
		break;
	}
	factory_config_block.ModelNumber[31] = '\0';

#if defined(CONFIG_SPL_BUILD)
	if (!rv)
		printf
		    ("MitySOM335x profile %d - Model No: %s Serial No: %d\n",
		     profile, factory_config_block.ModelNumber,
		     factory_config_block.SerialNumber);
#endif

	return rv;
}

#if defined(CONFIG_SPL_BUILD) && defined(CONFIG_SPL_BOARD_INIT)

void am33xx_spl_board_init(void)
{
	int ii = 0;
	uchar buf[4];
	int sil_rev;
	int mpu_vdd;
	tfenter;

	/* Get the frequency */
	dpll_mpu_opp100.m = am335x_get_efuse_mpu_max_freq(cdev);

#ifdef CONFIG_SPL_USBETH_SUPPORT
	mem_malloc_init(0x82000000, 0x00100000);
	set_default_env(NULL);
	printf("U-Boot Compiled for USB Boot Support\n");
#endif

#ifdef CONFIG_SPL_MMC_SUPPORT
	enable_mmc0_pin_mux();
#endif

	printf
	    ("Critical Link AM335X %s -- NAND Page size = %dk booting from dev %x\n",
	     BOARD_NAME,
	     nand_page_size + 1 / 1024, spl_boot_device());

	/* Prints serial and part number of SOM. Placed here because
	 * print statements on get_dpll_ddr_params are not working */
	read_eeprom();

	/*
	 * EVM PMIC code.  All boards currently want an MPU voltage
	 * of 1.2625V and CORE voltage of 1.1375V to operate at
	 * 720MHz.
	 */
	i2c_set_bus_num(i2c_tps65910_ctl_bus);	/* calls i2c_init... */
	if (i2c_probe(TPS65910_CTRL_I2C_ADDR)) {
		printf("No PMIC At I2C addr %d\n", TPS65910_CTRL_I2C_ADDR);
		for (; ii < 128; ++ii)
			if (0 == i2c_probe(ii))
				printf("I2C device found at addr %d\n", ii);
		return;
	}

	set_led_d3(1);

	/* Turn off pull down resistors for BOOT0P, BOOT1P.  Som has
	 * pullup resistor which wastes power. */
	buf[0] = 0;
	if(0 != i2c_read(TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG, 1, buf, 1)) {
		printf("Unable to read I2C reg %d:%d.%d %x\n",
		       i2c_get_bus_num(), TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG,
		       buf[0]);
	}
	buf[0] &= TPS65910_PUADEN_REG_BOOTP_PD_DISABLE;

	if (0 != i2c_write(TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG, 1, buf, 1)) {
		printf("Unable to write I2C reg %d:%d.%d %x\n",
		       i2c_get_bus_num(), TPS65910_CTRL_I2C_ADDR, TPS65910_PUADEN_REG,
		       buf[0]);
	}


	/* Get the frequency */
	dpll_mpu_opp100.m = am335x_get_efuse_mpu_max_freq(cdev);

	/*
	 * Depending on MPU clock and PG we will need a different
	 * VDD to drive at that speed.
	 */
	sil_rev = readl(&cdev->deviceid) >> 28;
	mpu_vdd = am335x_get_tps65910_mpu_vdd(sil_rev,
			dpll_mpu_opp100.m);

	/* Tell the TPS65910 to use i2c */
	tps65910_set_i2c_control();

	/* First update MPU voltage. */
	if (tps65910_voltage_update(MPU, mpu_vdd))
		return;

	/* Second, update the CORE voltage. */
	if (tps65910_voltage_update(CORE, TPS65910_OP_REG_SEL_1_1_3))
		return;

	/* Set CORE Frequencies to OPP100 */
	do_setup_dpll(&dpll_core_regs, &dpll_core_opp100);

	/* Set MPU Frequency to what we detected now that voltages are set */
	do_setup_dpll(&dpll_mpu_regs, &dpll_mpu_opp100);

	/* Switch NAND default configuration so we can load u-boot from NAND */
	//switch (factory_config_block.ModelNumber[NAND_SIZE_POSITION]) {
	//case NAND_SIZE_256MB:
	//	omap_nand_switch_ecc(1, 8);	/* HW-BCH8 ECC */
	//	break;
	//case NAND_SIZE_512MB:
	//	omap_nand_switch_ecc(1, 16);	/* HW-BCH16 ECC */
	//	break;
	//default:
	//	omap_nand_switch_ecc(0, 0);	/* SW ECC */
	//	break;
	//}

#if defined(CONFIG_SPL_USBETH_SUPPORT) && defined(CONFIG_SPL_BUILD)
	miiphy_init();
	board_eth_init(NULL);
#endif

}
#endif

#ifdef CONFIG_SPL_BUILD
static void prompt_for_mem(void)
{
	int OK;
	tfenter;

	printf("board_init: unable to read eeprom\n");
	do {
		OK = 1;
		printf("Enter Memory Config:\n"
		       "	0: DDR2\n"
		       "	1: DDR3 (256 MB)\n"
		       "	2: DDR3 (512 MB)\n" "	3: DDR3 (1 GB)\n");
		switch (serial_getc()) {
		case '0':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_256MB_DDR2;
			break;
		case '1':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_256MB_DDR3;
			break;
		case '2':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_512MB_DDR3;
			break;
		case '3':
			factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
			    RAM_SIZE_1GB_DDR3;
			break;
		default:
			OK = 0;
			printf("Invalid code!\n");
			break;
		}
	} while (!OK);
}

/* Used when setting up memory.  If config is uncertain, trigger a mtest */
static bool __attribute__((section (".data"))) need_mtest = false;

#define OSC	(V_OSCK/1000000)
const struct dpll_params dpll_ddr2 = {
		266, OSC-1, 1, -1, -1, -1, -1};
const struct dpll_params dpll_ddr3 = {
		400, OSC-1, 1, -1, -1, -1, -1};

/* Return ddr bus speed parameters */
const struct dpll_params *get_dpll_ddr_params(void)
{
	tfenter;

	/* set these up to skip reading from EEPROM and just force the config */
#ifdef  CONFIG_ASK_RAM
#if	defined(CONFIG_DDR3_256MB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
	    RAM_SIZE_256MB_DDR3;
#elif	defined(CONFIG_DDR3_512MB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
	    RAM_SIZE_512MB_DDR3;
#elif	defined(CONFIG_DDR3_1GB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] = RAM_SIZE_1GB_DDR3;
#elif	defined(CONFIG_DDR2_256MB)
	factory_config_block.ModelNumber[RAM_SIZE_POSITION] =
	    RAM_SIZE_256MB_DDR2;
#else
	/* if none selected, then prompt every time */
	printf("CONFIG_ASK_RAM: Always prompt for memory\n");
	prompt_for_mem();
#endif /* CONFIG_DDR* */
	need_mtest = true;
#else /* !CONFIG_ASK_RAM */
	/* read factory configuration number to determine memory type */
	enable_i2c1_pin_mux();
	if (0 != read_eeprom()) {
		/* bad read, make sure data is "bogus" for logic below */
		factory_config_block.ConfigVersion = 0;
		factory_config_block.ModelNumber[RAM_SIZE_POSITION] = ' ';
	}
	else {
		check_nand_support();
		configure_tps65910_ctrl_bus();
	}

	/* if old configuration, this has to be a DDR2 256 MB part,
	 * we need this check because a handful of units were not
	 * configured with the proper DDR code during the initial
	 * build and some units made it into the wild for early
	 * access customers */
	if (factory_config_block.ConfigVersion == CONFIG_I2C_VERSION_1_1) {
		printf("Old factory config found, "
		       "configuring for 256 MB DDR2\n");
		factory_config_block.ModelNumber[RAM_SIZE_POSITION] = RAM_SIZE_256MB_DDR2;
	}

	switch (factory_config_block.ModelNumber[RAM_SIZE_POSITION]) {
	case RAM_SIZE_256MB_DDR3:
	case RAM_SIZE_512MB_DDR3:
	case RAM_SIZE_1GB_DDR3:
	case RAM_SIZE_256MB_DDR2:
		break;
	default:
		prompt_for_mem();
		need_mtest = true;
		break;
	}
#endif /* CONFIG_ASK_RAM */

	switch(factory_config_block.ModelNumber[RAM_SIZE_POSITION]) {
	case RAM_SIZE_256MB_DDR3:
	case RAM_SIZE_512MB_DDR3:
	case RAM_SIZE_1GB_DDR3:
		return &dpll_ddr3;
	case RAM_SIZE_256MB_DDR2:
	default:
		return &dpll_ddr2;
	}
}

void set_uart_mux_conf(void)
{
	tfenter;
	enable_uart0_pin_mux();
}

void set_mux_conf_regs(void)
{
	tfenter;
	/* Configure the i2c1 and 2 pin mux */
	enable_i2c1_pin_mux();

	configure_evm_pin_mux(profile);
}

void sdram_init(void) {
	char ram_type;
	int bus_speed_ddr2 = 266;
	int bus_speed_ddr3 = 400;

	tfenter;
	ram_type = factory_config_block.ModelNumber[RAM_SIZE_POSITION];

	/* get the type from the position in RAM */
	switch (ram_type) {
	case RAM_SIZE_512MB_DDR3:
		printf("Configuring for 512 MB DDR3 @ %dMHz\n", bus_speed_ddr3);
		config_am335x_ddr3(bus_speed_ddr3, ram_type);
		break;
	case RAM_SIZE_256MB_DDR2:
		printf("Configuring for 256 MB DDR2 @ %dMHz\n", bus_speed_ddr2);
		config_am335x_ddr2(bus_speed_ddr2);
		break;
	case RAM_SIZE_256MB_DDR3:
		printf("Configuring for 256 MB DDR3 @ %dMHz\n", bus_speed_ddr3);
		config_am335x_ddr3(bus_speed_ddr3, ram_type);
		break;
	case RAM_SIZE_1GB_DDR3:
		printf("Configuring for 1024 MB DDR3 @ %dMHz\n",
		       bus_speed_ddr3);
		config_am335x_ddr3(bus_speed_ddr3, ram_type);
		break;
	default:		/* for now as we get boards to test this should
				   become NOT_REACHED */
		printf("Don't know how to handle memory type!\n");
		need_mtest = true;
		break;
	}
	if (256 * 1024 * 1024 > ram_size)
		ram_size = 256 * 1024 * 1024;
	if (need_mtest) {
		int mtest_ret = 0;
#if 0   /* Test whole ram */
		ulong start_addr = CONFIG_SYS_SDRAM_BASE;
		ulong end_addr = CONFIG_SYS_SDRAM_BASE + ram_size;
#else   /* Test 1k ram */
		ulong start_addr = CONFIG_SYS_MEMTEST_START;
		ulong end_addr = start_addr + 1024;
#endif

#ifdef CONFIG_AM335X_TF
		printf("TESTFIXTURE:Testing early memory\n");
#endif

		printf("ram size = %luMB, start = %08lx end = %08lx\n",
		       ram_size / 1024, start_addr,
		       end_addr);

		// Enable ctrlc handling
		disable_ctrlc(0);

#if defined(CONFIG_SYS_ALT_MEMTEST)
		mtest_ret = mem_test_alt(start_addr, end_addr);
#else
		mtest_ret = mtest((ulong *) start_addr, (ulong *) end_addr);
#endif

		disable_ctrlc(1);

		if (mtest_ret) {	/* mtest failed */
#ifdef CONFIG_AM335X_TF
			printf("TESTFIXTURE:FINISH:FAIL:ERR1-3 Memory test failed\n");
#endif
			printf("Memory Test failure.. board defect!\n");
			hang();
		}
#ifdef CONFIG_AM335X_TF
		else
		{
			printf("TESTFIXTURE:FINISH:SUCCESS\n");
		}
#endif
	}

}
#endif /* CONFIG_SPL_BUILD */

int board_init(void)
{
	tfenter;

#if defined(CONFIG_HW_WATCHDOG)
	hw_watchdog_init();
#endif

#ifndef CONFIG_SPL_BUILD
	if (0 != read_eeprom()) {
		printf("board_init:unable to read eeprom\n");
	}
	else {
		check_nand_support();
		configure_tps65910_ctrl_bus();
	}
#endif

	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;

	gpmc_init();

	return 0;
}

int misc_init_r(void)
{
	tfenter;

	set_fdt_config();

	return 0;
}

#ifdef CONFIG_DRIVER_TI_CPSW
#ifndef CONFIG_SPL_BUILD
static void cpsw_control(int enabled)
{
	/* Enable/disable the RGMII2 port */

	tfenter;

	return;
}

static struct cpsw_slave_data cpsw_slaves[] = {
	{
	 .slave_reg_ofs = 0x208,
	 .sliver_reg_ofs = 0xd80,
#ifdef CONFIG_SPL_USBETH_SUPPORT
	 .phy_addr = 1,
#else
	 .phy_addr = 2,
#endif
	 },
	{
	 .slave_reg_ofs = 0x308,
	 .sliver_reg_ofs = 0xdc0,
#ifdef CONFIG_SPL_USBETH_SUPPORT
	 .phy_addr = 2,
#else
	 .phy_addr = 1,
#endif
	 },
};

static struct cpsw_platform_data cpsw_data = {
	.mdio_base = CPSW_MDIO_BASE,
	.cpsw_base = CPSW_BASE,
	.mdio_div = 0xff,
	.channels = 8,
	.cpdma_reg_ofs = 0x800,
	.slaves = 2,
	.slave_data = cpsw_slaves,
	.ale_reg_ofs = 0xd00,
	.ale_entries = 1024,
	.host_port_reg_ofs = 0x108,
	.hw_stats_reg_ofs = 0x900,
	.bd_ram_ofs = 0x2000,
	.mac_control = (1 << 5) /* MIIEN */ ,
	.control = cpsw_control,
	.host_port_num = 0,
	.version = CPSW_CTRL_VERSION_2,
	.active_slave = CONFIG_ETH_PORT - 1,
};
#endif /* !CONFIG_SPL_BUILD */

int board_eth_init(bd_t *bis)
{
	int n = 0;
#if !defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_USBETH_SUPPORT)
	int rv = 0;
	uint8_t mac_addr[6];
	uint32_t mac_hi, mac_lo;
	int valid_mac = 0;

	tfenter;

#ifdef CONFIG_MITYSOM_MAKER
	env_set("disable_giga", "1");
#endif

	gpio_request(GPIO_PHY_RESET_N, "gpio3_10");
	// Pull into reset
	gpio_direction_output(GPIO_PHY_RESET_N, 0);
	udelay(100);
	// Pull out of reset
	gpio_set_value(GPIO_PHY_RESET_N, 1);

	/*
	 * Environment set MAC address trumps all...
	 */
	if (eth_env_get_enetaddr("ethaddr", mac_addr))
		valid_mac = 1;
	/*
	 * Factory configuration MAC address overides built-in
	 */
	else if (is_valid_ethaddr(factory_config_block.MACADDR)) {
		memcpy(mac_addr, factory_config_block.MACADDR, 6);
		eth_env_set_enetaddr("ethaddr", mac_addr);
		valid_mac = 1;
	}
	/* Fall back to MAC address in the eFuse eeprom in part */
	else {

		debug("<ethaddr> not set. Reading from E-fuse\n");
		/* try reading mac address from efuse */
#if (CONFIG_ETH_PORT == 1)
		mac_lo = readl(&cdev->macid0l);
		mac_hi = readl(&cdev->macid0h);
#else
		mac_lo = readl(&cdev->macid1l);
		mac_hi = readl(&cdev->macid1h);
#endif
		mac_addr[0] = mac_hi & 0xFF;
		mac_addr[1] = (mac_hi & 0xFF00) >> 8;
		mac_addr[2] = (mac_hi & 0xFF0000) >> 16;
		mac_addr[3] = (mac_hi & 0xFF000000) >> 24;
		mac_addr[4] = mac_lo & 0xFF;
		mac_addr[5] = (mac_lo & 0xFF00) >> 8;

		if (is_valid_ethaddr(mac_addr)) {
			eth_env_set_enetaddr("ethaddr", mac_addr);
			valid_mac = 1;
		}
	}

#if defined(CONFIG_USB_ETHER) && \
	(!defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_USBETH_SUPPORT))
	if (is_valid_ethaddr(mac_addr)){
		eth_env_set_enetaddr("usbnet_devaddr", mac_addr);
		eth_env_set_enetaddr("eth1addr", mac_addr);
	}
	rv = usb_eth_initialize(bis);
	if (rv < 0)
		printf("Error %d registering USB_ETHER\n", rv);
	else
		n += rv;
#endif
#if !defined(CONFIG_SPL_BUILD)
	if (valid_mac)
	{
		writel(RGMII_MODE_ENABLE, &cdev->miisel);
		cpsw_slaves[0].phy_if = cpsw_slaves[1].phy_if =
				PHY_INTERFACE_MODE_RGMII;
	}
	else
	{
		printf("Warning: MAC Address not found. Ethernet disabled\n");
	}

	rv = cpsw_register(&cpsw_data);
	if (rv < 0)
		printf("Error %d registering CPSW switch\n", rv);
	else
		n += rv;
#endif
#endif /* !defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_USBETH_SUPPORT) */
	return n;
}
#endif /* CONFIG_DRIVER_TI_CPSW */

#ifdef CONFIG_SPL_LOAD_FIT
int board_fit_config_name_match(const char *name)
{
	if (!strcmp(name, "mitysom-335x"))
		return 0;
	else
		return -1;
}
#endif /* CONFIG_SPL_LOAD_FIT */

/* FDT system fix-up functions */
#if defined(CONFIG_OF_LIBFDT) && defined(CONFIG_OF_SYSTEM_SETUP)
/*
 * **This function was copied from fdt_overlay.c**
 *
 * overlay_apply_node - Merges a node into the base device tree
 * @fdt: Base Device Tree blob
 * @target: Node offset in the base device tree to apply the fragment to
 * @fdto: Device tree overlay blob
 * @node: Node offset in the overlay holding the changes to merge
 *
 * overlay_apply_node() merges a node into a target base device tree
 * node pointed.
 */
static int overlay_apply_node(void *fdt, int target,
                              void *fdto, int node)
{
	int property;
	int subnode;

	fdt_for_each_property_offset(property, fdto, node) {
		const char *name;
		const void *prop;
		int prop_len;
		int ret;

		prop = fdt_getprop_by_offset(fdto, property, &name,
					     &prop_len);
		if (prop_len == -FDT_ERR_NOTFOUND)
			return -FDT_ERR_INTERNAL;
		if (prop_len < 0)
			return prop_len;

		ret = fdt_setprop(fdt, target, name, prop, prop_len);
		if (ret)
			return ret;
	}

	fdt_for_each_subnode(subnode, fdto, node) {
		const char *name = fdt_get_name(fdto, subnode, NULL);
		int nnode;
		int ret;

		nnode = fdt_add_subnode(fdt, target, name);
		if (nnode == -FDT_ERR_EXISTS) {
			nnode = fdt_subnode_offset(fdt, target, name);
			if (nnode == -FDT_ERR_NOTFOUND)
				return -FDT_ERR_INTERNAL;
		}

		if (nnode < 0)
			return nnode;

		ret = overlay_apply_node(fdt, nnode, fdto, subnode);
		if (ret)
			return ret;
	}

	return 0;
}

/*
 * merge_nodes: Merges one node into another.
 * @fdt: Device tree blob
 * @source: Node offset in blob to merge from
 * @destination: Node offset in blob to merge to
 *
 * Merges the Source node into the Destination node. This process may
 * effect offset of nodes calculated before this function.
 */
static int merge_nodes(void *fdt, int source, int destination)
{
	char swap_fdt[CL_SWAP_TREE_SIZE];
	int ret;
	int node;

	/* Make a temporary FDT to hold data */
	ret = fdt_create_empty_tree(swap_fdt, CL_SWAP_TREE_SIZE);
	if (ret)
		return ret;

	/* Add a node to hold the source data */
	node = fdt_add_subnode(swap_fdt, 0, "swap");
	if (node < 0)
		return node;

	/* Copy the source node into the temp tree */
	ret = overlay_apply_node(swap_fdt, node, fdt, source);
	if (ret)
		return ret;

	node = fdt_path_offset(swap_fdt, "/swap");
	if (node < 0)
		return node;

	/* Increase the size of the original fdt to fit the data */
	ret = fdt_shrink_to_minimum(fdt, CL_SWAP_TREE_SIZE);
	if (ret < 0)
		return ret;

	/* Copy the node back into the original tree */
	return overlay_apply_node(fdt, destination, swap_fdt, node);
}

int ft_fixup_cl_nand(void *blob)
{
	tfenter;
	int ret;
	int gpmc_orig_node, gpmc_chosen_node = -1;
	char *unit_name;

	switch(factory_config_block.ModelNumber[NAND_SIZE_POSITION]) {
	case NAND_SIZE_256MB:
		unit_name = CL_FIXUP_GPMC_FIXUP_256MB;
		break;
	case NAND_SIZE_512MB:
		unit_name = CL_FIXUP_GPMC_FIXUP_512MB;
		break;
	case NAND_SIZE_1GB:
		unit_name = CL_FIXUP_GPMC_FIXUP_1024MB;
		break;
	case NAND_SIZE_NONE:
		printf("Unrecognized NAND size: \"%c\"\n", factory_config_block.ModelNumber[NAND_SIZE_POSITION]);
		/* If the chosen unit's name is null, we'll skip find/renaming it */
		unit_name = '\0';
	default:
		/* Nothing to do */
		return 0;
	}

	/* Enable the chosen NAND node */
	gpmc_chosen_node = fdt_path_offset(blob, unit_name);
	if (gpmc_chosen_node < 0)
		return gpmc_chosen_node;

	ret = fdt_setprop_string(blob, gpmc_chosen_node, "status", "okay");
	if (ret)
		return ret;

	/* Find the original node */
	gpmc_orig_node = fdt_path_offset(blob, CL_FIXUP_GPMC_UNIT_NAME);
	if (gpmc_orig_node < 0) {
		return gpmc_orig_node;
	}

	ret = merge_nodes(blob, gpmc_chosen_node, gpmc_orig_node);

	return ret;
}

int ft_fixup_cl_nor(void *blob)
{
	tfenter;
	int ret;
	int node, conf_node;
	char *unit_name;

	/* Determine if we have NOR */
	switch(factory_config_block.ModelNumber[NOR_SIZE_POSITION]) {
	case NOR_SIZE_8MB:
		unit_name = CL_FIXUP_NOR_FIXUP_8MB;
		break;
	default:
		printf("Unsupported NOR size: %c\n", factory_config_block.ModelNumber[NOR_SIZE_POSITION]);
	case NOR_SIZE_NONE:
		return 0;
	}

	/* Make sure we have the NOR config node */
	conf_node = fdt_path_offset(blob, unit_name);
	if (conf_node < 0)
		return conf_node;

	/* Try to delete the NAND placeholder */
	node = fdt_path_offset(blob, CL_FIXUP_NOR_PLACEHOLDER_PATH);
	if (node > 0) {
		ret = fdt_del_node(blob, node);
		printf("ret = %d\n", ret);
		if (ret)
			return ret;
	}

	/* Load nodes and merge */
	conf_node = fdt_path_offset(blob, unit_name);
	if (conf_node < 0)
		return conf_node;

	node = fdt_path_offset(blob, CL_FIXUP_NOR_UNIT_NAME);
	if (node < 0)
		return node;

	return merge_nodes(blob, conf_node, node);
}

int ft_fixup_cl_cleanup(void *blob)
{
	tfenter;
	int node;

	/* Find the config node */
	node = fdt_path_offset(blob, CL_FIXUP_PATH);
	if (node < 0)
		return node;

	/* Delete it */
	return fdt_del_node(blob, node);
}

int ft_system_setup(void *blob, bd_t *bd)
{
	tfenter;
	int ret;

	ret = fdt_path_offset(blob, CL_FIXUP_PATH);
	if (ret < 0) {
		return 0;
	}

	ret = ft_fixup_cl_nand(blob);
	if (ret) {
		printf("NAND fix-up failed! Aborting system setup!\n");
		return ret;
	}

	ret = ft_fixup_cl_nor(blob);
	if (ret) {
		printf("NOR fix-up failed! Aborting system setup!\n");
		return ret;
	}

	ret = ft_fixup_cl_cleanup(blob);
	if (ret) {
		printf("Clean up failed!\n");
	}

	return ret;
}
#endif /* CONFIG_OF_LIBFDT && CONFIG_OF_SYSTEM_SETUP */

#ifndef CONFIG_SPL_BUILD

#ifdef CONFIG_BOARD_LATE_INIT
int board_late_init(void)
{
	tfenter;

	/* Turn diagnostic led off to indicate configuration complete */
	set_led_d3(0);

	/* TODO Could set environment variables such as board model number,
	 * serial number.  See am335x_evm/board.c for example */
#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	char safe_string[64];

	env_set("board_name", "MitySOM335x");

	//strncpy(safe_string, (char *)factory_config_block., sizeof(header.version));
	//safe_string[sizeof(header.version)] = 0;
	//env_set("board_rev", safe_string);

	strncpy(safe_string, (char *)factory_config_block.ModelNumber, sizeof(factory_config_block.ModelNumber));
	safe_string[sizeof(factory_config_block.ModelNumber)] = 0;
	env_set("board_model", safe_string);

	sprintf(safe_string, "%d", factory_config_block.SerialNumber);
	env_set("board_serialno", safe_string);
#endif
	return 0;
}
#endif /* CONFIG_BOARD_LATE_INIT */
#endif /* CONFIG_SPL_BUILD */
